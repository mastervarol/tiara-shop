@extends('template.merchant_template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('merchant.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - All Review
    </h4>
@endpush
@push('top_right_button')
    
@endpush
@section('content')
<div class="content">
    <!-- Alert -->
    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
    </div>
    @endif
    <!-- /Alert -->
    <div class="card">
        {{-- <div class="card-header header-elements-inline">
            <h5 class="card-title">User List</h5>
            <div class="header-elements">
                <a href="{{ route('admin.user.create', []) }}"><i class="icon-plus3"></i> New</a>
            </div>
        </div> --}}
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th class="wmin-md-10">#</th>
                        <th class="wmin-md-30">Review By</th>
                        <th class="wmin-md-30">Rate</th>
                        <th class="wmin-md-30">Comment</th>
                        <th class="wmin-md-10">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 0;
                    @endphp
                    @foreach ($data as $item)
                    @php
                        $no++;
                    @endphp
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $item->member->name }}</td>
                        <td>
                            @for ($i = 0; $i < $item->review; $i++)
                                <i class="icon-star-full2"></i>
                            @endfor
                        </td>
                        <td>{{ Str::words($item->comment, 12) }}</td>
                        <td>
                            <div class="text-center">
		                    	<a href="{{ route('merchant.review.show', [$item->id]) }}" class="btn bg-teal-400 btn-icon rounded-round">
                                    <b><i class="icon-eye"></i></b>
                                </a>
	                    	</div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <ul class="pagination pagination-flat">
        {{ $data->links() }}
    </ul>
</div>
@endsection
@push('additional_js_plugin')
    
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-review').addClass('active');
        });
    </script>
@endpush