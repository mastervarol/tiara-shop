@extends('template.merchant_template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('merchant.transaction', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Review Detail
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('merchant.review', []) }}" class="btn btn-link btn-float text-default">
        <i class="icon-backward2 text-danger"></i> 
        <span>Back</span>
    </a>
@endpush
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header bg-teal-400 text-white header-elements-inline">
                    <h6 class="card-title">
                        @for ($i = 0; $i < $data->review; $i++)
                            <i class="icon-star-full2"></i>
                        @endfor
                    </h6>
                    <small class="font-weight-thin ml-1 pull-right">{{ $data->createdDate() }}</small>
                </div>
                
                <div class="card-body">
                    <blockquote class="blockquote blockquote-bordered py-2 pl-3 mb-0">
                        <p class="mb-1">{{ $data->comment }}</p>
                        <footer class="blockquote-footer">by <cite title="Source Title">{{ $data->member->name }}</cite></footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('additional_js_plugin')
<script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-review').addClass('active');
        });
    </script>
@endpush