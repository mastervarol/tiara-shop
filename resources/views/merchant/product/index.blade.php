@extends('template.merchant_template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('merchant.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Product List
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('merchant.product.create', []) }}" class="btn btn-link btn-float text-default">
        <i class="icon-plus3 text-primary"></i> 
        <span>Add New</span>
    </a>
@endpush
@section('content')
<div class="content">
    <!-- Alert -->
    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
    </div>
    @endif
    <!-- /Alert -->
    <div class="card">
        {{-- <div class="card-header header-elements-inline">
            <h5 class="card-title">User List</h5>
            <div class="header-elements">
                <a href="{{ route('admin.user.create', []) }}"><i class="icon-plus3"></i> New</a>
            </div>
        </div> --}}
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th class="wmin-md-10">#</th>
                        <th class="wmin-md-150">Photo</th>
                        <th class="wmin-md-30">Name</th>
                        <th class="wmin-md-30">Type</th>
                        <th class="wmin-md-30">Prices</th>
                        <th class="wmin-md-10">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 0;
                    @endphp
                    @foreach ($data as $item)
                    @php
                        $no++;
                    @endphp
                    <tr>
                        <td>{{ $no }}</td>
                        <td>
                            <div class="mr-3">
                                <div class="mr-sm-3 mb-2 mb-sm-0">
                                    <div class="card-img-actions">
                                        <a href="{{ asset('/global_assets/images/product/'. $item->image) }}" target="_blank">
                                            <img src="{{ asset('/global_assets/images/product/'. $item->image) }}" class="rounded-circle" width="100" height="100" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <strong>{{ $item->name }}</strong><br>
                            <p>
                                {{ Str::words($item->description, 15, '..') }}
                            </p>
                        </td>
                        <td>{{ $item->type() }}</td>
                        <td>
                            @if ($item->discount_price)
                                <s>{{ $item->sellPrice() }}</s><br>
                                <strong>{{ $item->discountPrice() }}</strong>
                            @else 
                                {{ $item->sellPrice() }}
                            @endif
                        </td>
                        <td>
                            <div class="btn-group justify-content-center">
                                <a href="#" class="btn bg-indigo-400 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options</a>

                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);">
                                    <a href="{{ route('merchant.product.edit', [$item->id]) }}" class="dropdown-item"><i class="icon-pencil7"></i> Edit</a>
                                    <div class="dropdown-divider"></div>
                                    <form action="{{ route('merchant.product.delete', [$item->id]) }}" method="POST" enctype="multipart/form-data">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="dropdown-item text-pink" onclick="return confirmDeletion();"><i class="icon-trash"></i>Delete</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <ul class="pagination pagination-flat">
        {{ $data->links() }}
    </ul>
</div>
@endsection
@push('additional_js_plugin')
    
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-product').addClass('active');
        });

        function confirmDeletion(){
            return confirm('Are you sure want to delete this data?');
        }
    </script>
@endpush