@extends('template.merchant_template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('merchant.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - New Product
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('merchant.product', []) }}" class="btn btn-link btn-float text-default">
        <i class="icon-blocked text-danger"></i> 
        <span>Cancel</span>
    </a>
@endpush
@section('content')
<div class="content">
    @if ($errors->any())
        <!-- Alert -->
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Oh snap!</span> Change a few things up and <a href="#" class="alert-link">try submitting again</a>.
            <br>
            <ul class="mt-3">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <!-- /Alert -->
    @endif
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">New Product Form</h5>
        </div>

        <div class="card-body">
            <form action="{{ route('merchant.product.store') }}" method="POST" enctype="multipart/form-data">
                @method('POST')
                @csrf
                <fieldset class="mb-6">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Image*</label>
                        <div class="col-lg-4">
                            <input type="file" name="image" id="image" class="form-control h-auto" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Name*</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Description*</label>
                        <div class="col-lg-4">
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control" required>{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Type*</label>
                        <div class="col-lg-4">
                            <select class="form-control form-control-uniform-custom" name="type" id="type" required>
                                <option value="" selected hidden disabled>Select</option>
                                <option value="0">Food</option>
                                <option value="1">Beverage</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Prices*</label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="sell_price" id="sell_price" value="{{ old('sell_price') }}" placeholder="Sell Price (e.g 10000)" required>
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="discount_price" id="discount_price" value="{{ old('discount_price') }}" placeholder="Discount Price (e.g 10000)">
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label text-lg-right"></label>
                    <div class="col-lg-4">
                        <button type="submit" id="submit" class="btn btn-primary pull-right">
                            Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                        {{-- <button type="submit" id="submit" class="btn btn-primary pull-right">Submit<i class="icon-paperplane ml-2"></i></button> --}}
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->

</div>
@endsection
@push('additional_js_plugin')
<script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            console.log('document ready');

            $('#menu-product').addClass('active');
        });
    </script>
@endpush