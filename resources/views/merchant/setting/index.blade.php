@extends('template.merchant_template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('admin.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Setting
    </h4>
@endpush
@push('top_right_button')
    
@endpush
@section('content')
<div class="content">
    @if ($errors->any())
        <!-- Alert -->
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Oh snap!</span> Change a few things up and <a href="#" class="alert-link">try submitting again</a>.
            <br>
            <ul class="mt-3">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <!-- /Alert -->
    @endif
    <!-- Alert -->
    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
    </div>
    @endif
    <!-- /Alert -->
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header bg-teal-400 text-white header-elements-inline">
            <h6 class="card-title">{{ Str::upper($data->name) }}</h6>
            <div class="header-elements">
                <div class="list-icons">
                    <i class="icon-star-full2"></i>{{ $data->review }}
                </div>
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('merchant.setting.update', [$data->id]) }}" method="POST" enctype="multipart/form-data">
                @method('POST')
                @csrf
                <fieldset class="mb-6">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card-body text-center">
                                <h6 class="font-weight-semibold mb-0">Merchant Logo</h6>
					    	</div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right"></label>
                                <div class="col-lg-8">
                                    <input type="file" name="merchant_logo" id="merchant_logo" class="file-input" value="{{ old('merchant_logo') }}" data-fouc>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Merchant Name*</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="merchant_name" id="merchant_name" value="{{ old('merchant_name', $data->name) }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Merchant Email*</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="merchant_email" id="merchant_email" value="{{ old('merchant_email', $data->email) }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Merchant Phone*</label>
                                    <div class="col-lg-8">
                                        <input type="tel" class="form-control" name="merchant_phone" id="merchant_phone" value="{{ old('merchant_phone', $data->phone) }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Whatsapp Available?*</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="has_whatsapp" id="has_whatsapp" required>
                                            <option value="" selected hidden disabled>Select</option>
                                            <option value="1" {{ $data->is_whatsapp == 1 ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ $data->is_whatsapp == 0 ? 'selected' : '' }}>No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Open?*</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="is_open" id="is_open" required>
                                            <option value="" selected hidden disabled>Select</option>
                                            <option value="1" {{ $data->is_open == 1 ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ $data->is_open == 0 ? 'selected' : '' }}>No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-body text-center">
					    		<h6 class="font-weight-semibold mb-0">Current Logo</h6>
					    	</div>
                            <div class="card">
                                <div class="card-img-actions">
                                    @if ($data->photo)
                                        <img class="card-img-top img-fluid" src="{{ asset('/global_assets/images/merchant/'. $data->photo) }}" alt="">
                                    @else 
                                        <img class="card-img-top img-fluid" src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}" alt="">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <label class="col-lg-1 col-form-label text-lg-right mr-3"></label>
                    <div class="col-lg-4">
                        <button type="submit" id="submit" class="btn btn-primary pull-right">
                            Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                        {{-- <button type="submit" id="submit" class="btn btn-primary pull-right">Submit<i class="icon-paperplane ml-2"></i></button> --}}
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->
</div>
@endsection
@push('additional_js_plugin')
    {{-- File input --}}
    <script src="{{ asset('/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/uploader_bootstrap.js') }}"></script>
    {{-- Tag input --}}
    <script src="{{ asset('/global_assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/forms/tags/tokenfield.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/ui/prism.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/demo_pages/form_tags_input.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-setting').addClass('active');
        });
    </script>
@endpush