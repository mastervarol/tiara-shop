@extends('template.merchant_template')
@push('additional_css_plugin')

@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('merchant.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - All Transaction List
    </h4>
@endpush
@push('top_right_button')

@endpush
@section('content')
    <div class="content">
        <!-- Alert -->
        @if (Session::has('message'))
            <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
            </div>
        @endif
        <!-- /Alert -->
        <div class="card">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="wmin-md-10">Code</th>
                            <th class="wmin-md-10">Ordered By</th>
                            <th class="wmin-md-30">Note</th>
                            <th class="wmin-md-10">Status</th>
                            <th class="wmin-md-10">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td>
                                    <span class="badge badge-{{ $item->status == 1 ? 'warning' : 'success' }} d-block">
                                        {{ $item->code }}
                                    </span>
                                </td>
                                <td>{{ $item->member->name }}</td>
                                <td>{{ Str::words($item->note, 15) }}</td>
                                <td>{{ $item->transactionStatus() }}</td>
                                <td>
                                    <div class="btn-group justify-content-center">
                                        <a href="#" class="btn bg-indigo-400 dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">Options</a>

                                        <div class="dropdown-menu" x-placement="bottom-start"
                                            style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);">
                                            @if ($item->status == 1)
                                                <form
                                                    action="{{ route('merchant.transaction.update', [$item->id, 'opt' => 'finish', 'page' => 'merchant']) }}"
                                                    method="POST">
                                                    @method('PUT')
                                                    @csrf
                                                    <button type="submit" class="dropdown-item"
                                                        onclick="return confirmFinish();"><i class="icon-touch"></i> Finish
                                                        Transaction</button>
                                                </form>
                                                <div class="dropdown-divider"></div>
                                            @endif
                                            <a href="{{ route('merchant.transaction.show', [$item->id, 'opt' => request()->opt, 'page' => 'merchant']) }}"
                                                class="dropdown-item"><i class="icon-eye"></i> Detail</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <ul class="pagination pagination-flat">
            {{ $data->links() }}
        </ul>
    </div>
@endsection
@push('additional_js_plugin')

@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#nav-transaction').addClass('nav-item-open');
            $('#nav-transaction ul').css('display', 'block');
            $('#menu-transaction-{{ request()->opt }}').addClass('active');
        });

        function confirmFinish() {
            return confirm('Confirm this transaction?');
        }

    </script>
@endpush
