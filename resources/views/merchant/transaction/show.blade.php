@extends('template.merchant_template')
@push('additional_css_plugin')

@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('merchant.transaction', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Transaction Detail
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('merchant.transaction', ['opt' => request()->opt]) }}" class="btn btn-link btn-float text-default">
        <i class="icon-backward2 text-danger"></i>
        <span>Back</span>
    </a>
@endpush
@section('content')
    <div class="content">
        <!-- Alert -->
        @if (Session::has('message'))
            <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
            </div>
        @endif
        <!-- /Alert -->
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card h-100">
                    <div class="card-header {{ $bg }} text-white header-elements-inline">
                        <h6 class="card-title">
                            <i class="icon-dots"></i> Detail
                        </h6>
                    </div>

                    <div class="card-body">
                        @if ($data->status == 1)
                            <div class="mt-0">
                                <form
                                    action="{{ route('merchant.transaction.update', [$data->id, 'opt' => 'finish', 'page' => request()->page]) }}"
                                    method="POST">
                                    @method('PUT')
                                    @csrf
                                    <button type="submit" class="btn btn-outline-danger btn-block"
                                        onclick="return confirmFinish();">
                                        <i class="icon-touch"></i> Finish Transaction
                                    </button>
                                </form>
                            </div>
                            <div class="dropdown-divider mt-3"></div>
                        @endif
                        <h2 class="mb-0 font-weight-light">
                            <small class="font-weight-light ml-1">
                                <i class="icon-loop4"></i> Transaction
                            </small>
                        </h2>
                        <div class="card card-body bg-light mb-0">
                            <dl class="row mb-0">
                                <dt class="col-sm-4">Code</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->code }}
                                </dd>

                                <dt class="col-sm-4">Merchant</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->merchant->name }}
                                </dd>

                                <dt class="col-sm-4">Cashier</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->cashier->name ?? '-' }}
                                </dd>

                                <dt class="col-sm-4 text-truncate">Member</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->member->name }}
                                </dd>

                                <dt class="col-sm-4">Status</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->transactionStatus() }}
                                </dd>
                            </dl>
                        </div>
                        <h2 class="mt-1 font-weight-light">
                            <small class="font-weight-light ml-1">
                                <i class="icon-credit-card"></i> Payment
                            </small>
                        </h2>
                        <div class="card card-body bg-light mb-0">
                            <dl class="row mb-0">
                                <dt class="col-sm-4">Status</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->paymentStatus() }}
                                </dd>

                                <dt class="col-sm-4">Type</dt>
                                <dd class="col-sm-8">
                                    : {{ $data->paymentType() }}
                                </dd>
                            </dl>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="card h-100">
                    <div class="card-header {{ $bg }} text-white header-elements-inline">
                        <h6 class="card-title">
                            <i class="icon-bookmark4"></i> Note
                        </h6>
                    </div>

                    <div class="card-body">
                        <blockquote class="blockquote blockquote-bordered py-2 pl-3 mb-0 overflow-auto">
                            <p class="mb-1">{{ $data->note }}.</p>
                            <footer class="blockquote-footer">a note by <cite
                                    title="Source Title">{{ $data->member->name }}</cite></footer>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header {{ $bg }} text-white header-elements-inline">
                        <h6 class="card-title">
                            <i class="icon-box"></i> Product List
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="wmin-md-10">Product</th>
                                        <th class="wmin-md-10">Qty</th>
                                        <th class="wmin-md-30">Price</th>
                                        <th class="wmin-md-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($data->items as $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->pivot->qty }}</td>
                                            <td>IDR {{ number_format($item->pivot->price, 2, ',', '.') }}</td>
                                            <td>IDR {{ number_format($item->pivot->total_price, 2, ',', '.') }}</td>
                                        </tr>
                                        @php
                                            $total += $item->pivot->total_price;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Total</th>
                                        <th>IDR {{ number_format($total, 2, ',', '.') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('additional_js_plugin')
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#nav-transaction').addClass('nav-item-open');
            $('#nav-transaction ul').css('display', 'block');
            $('#menu-transaction-{{ $opt }}').addClass('active');
        });

        function confirmFinish() {
            return confirm('Confirm mark this transaction as finish?');
        }

    </script>
@endpush
