@extends('template.member_template')
@push('additional_css_plugin')
    {{-- Animation --}}
    <link href="{{ asset('/global_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
@endpush
@push('second_header')
    {{-- <div class="page-title d-flex">
    <h4><i class="icon-spotlight2 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div> --}}
@endpush
@section('content')
    <!-- Content area -->
    <div class="content">
        <!-- Alert -->
        @if (Session::has('message'))
            <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
            </div>
        @endif
        <div class="row">
            <!-- Customers -->
            <div class="col-md-12 col-xl-12">
                <div class="card">
                    <div class="card-header bg-teal-400 text-white header-elements-inline">
                        <h6 class="card-title">Product List</h6>
                    </div>
                    <form action="{{ route('member.checkout.proceed', ['id' => $data->id]) }}" method="POST">
                        <div class="card-body">
                            @method('POST')
                            @csrf
                            <table class="table table-striped text-nowrap table-customers">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data->carts as $item)
                                        <tr>
                                            <td>
                                                <div class="media">
                                                    <div class="mr-3">
                                                        <a href="{{ route('member.product.show', ['id' => 1]) }}">
                                                            <img src="{{ asset('/global_assets/images/product/' . $item->image) }}"
                                                                width="40" height="40" class="rounded-circle" alt="">
                                                        </a>
                                                    </div>

                                                    <div class="media-body align-self-center">
                                                        <a href="{{ route('member.product.show', ['id' => 1]) }}"
                                                            class="font-weight-semibold">{{ $item->name }}</a>
                                                        <div class="text-muted font-size-sm">
                                                            Latest order: 2016.12.30
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="hidden" name="item[]" value="{{ $item->id }}">
                                                <input type="number" class="form-control ui-autocomplete-input" name="qty[]"
                                                    id="qty" style="max-width: 75px;" min="1"
                                                    value="{{ $item->qty ?? 1 }}" autocomplete="off">
                                                <h6 class="mb-0 font-weight-semibold">{{ $item->pivot->qty }}</h6>
                                            </td>
                                            <td>
                                                <a href="{{ route('member.removeItem', ['id' => Auth::user()->member->id, 'pid' => $item->id]) }}"
                                                    onclick="return confirm('Delete item?');" class="btn btn-danger">
                                                    <i class="icon-trash"></i></a>
                                                {{-- <form
                                                    action="{{ route('member.removeItem', ['id' => Auth::user()->id, 'pid' => $item->id]) }}"
                                                    method="POST">
                                                    @method('POST')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger"
                                                        onclick="return confirm('Delete item?');"><i
                                                            class="icon-trash"></i></button>
                                                </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control form-control-select2" name="payment_type"
                                            aria-hidden="true" required>
                                            <option value="" selected hidden>Choose Payment</option>
                                            <option value="1">Cash</option>
                                            {{-- <option value="0">Credit Card</option> --}}
                                            <option value="2">E-Payment</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="5" cols="5" name="note" class="form-control"
                                            placeholder="Enter your message here"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        @if (count($data->carts) > 0)
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-primary">Checkout <i
                                                        class="icon-paperplane ml-2"></i></button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /customers -->
    </div>

    </div>
    <!-- /content area -->
@endsection
@push('additional_js_plugin')
    {{-- Animation --}}
    <script src="{{ asset('/global_assets/js/demo_pages/animations_css3.js') }}"></script>
    {{-- Sweet Alert --}}
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js', []) }}"></script>
    {{-- Datatable --}}
    <script src="{{ asset('/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    {{-- Select2 --}}
    <script src="{{ asset('/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- <script src="{{ asset('/global_assets/js/demo_pages/form_layouts.js') }}"></script> --}}
    {{-- Axios --}}
    <script src="{{ url('https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js', []) }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-checkout').addClass('active');
        });

    </script>
@endpush
