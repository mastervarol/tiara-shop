@extends('template.member_template')
@push('additional_css_plugin')
    {{-- Animation --}}
    <link href="{{ asset('/global_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
@endpush
@push('page_header')
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="header-elements pull-right">
            <div class="breadcrumb justify-content-center">
                <div class="breadcrumb-elements-item dropdown p-0">
                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false">
                        <i class="icon-gear mr-2"></i>
                        Member Menu
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                        style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(96px, 40px, 0px);">
                        <a href="#cart" class="dropdown-item nav-link" data-toggle="tab"><i class="icon-cart4"></i> Cart</a>
                        <a href="#history" class="dropdown-item nav-link" data-toggle="tab"><i class="icon-calendar3"></i>
                            History</a>
                        <a href="#wishlist" class="dropdown-item nav-link" data-toggle="tab"><i class="icon-heart5"></i>
                            Wishlist</a>
                        <a href="#review" class="dropdown-item nav-link" data-toggle="tab"><i class="icon-star-full2"></i>
                            Review</a>
                        <div class="dropdown-divider"></div>
                        <a href="#setting" class="dropdown-item nav-link" data-toggle="tab"><i class="icon-gear"></i>
                            Account settings</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush
@section('content')
    <div class="content">
        <!-- Alert -->
        @if (Session::has('message'))
            <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
            </div>
        @endif
        <!-- Inner container -->
        <div class="d-md-flex align-items-md-start">

            <!-- Left sidebar component -->
            <div
                class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left wmin-300 border-0 shadow-0 sidebar-expand-md">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- Navigation -->
                    <div class="card">
                        <div class="card-body bg-indigo-400 text-center card-img-top"
                            style="background-image: url({{ asset('/global_assets/images/backgrounds/panel_bg.png') }}); background-size: contain;">
                            <div class="card-img-actions d-inline-block mb-3">
                                <img class="img-fluid rounded-circle"
                                    src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}" width="170"
                                    height="170" alt="">
                                <div class="card-img-actions-overlay rounded-circle">
                                    <a href="#"
                                        class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
                                        <i class="icon-plus3"></i>
                                    </a>
                                    <a href="user_pages_profile.html"
                                        class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                        <i class="icon-link"></i>
                                    </a>
                                </div>
                            </div>

                            <h6 class="font-weight-semibold mb-0">{{ $data->name }}</h6>
                            <span class="d-block opacity-75">{{ $data->points }} Pts</span>

                            {{-- <div class="list-icons list-icons-extended mt-3">
                            <a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Google Drive"><i class="icon-google-drive"></i></a>
                            <a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Twitter"><i class="icon-twitter"></i></a>
                            <a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Github"><i class="icon-github"></i></a>
                        </div> --}}
                        </div>

                        <div class="card-body p-0">
                            <ul class="nav nav-sidebar mb-2">
                                <li class="nav-item-header">Navigation</li>
                                <li class="nav-item">
                                    <a href="#cart" class="nav-link active" data-toggle="tab">
                                        <i class="icon-cart4"></i>
                                        Cart
                                        @if (count($data->carts) > 0)
                                            <span class="badge bg-success badge-pill ml-auto">
                                                {{ count($data->carts) }}
                                            </span>
                                        @endif
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#history" class="nav-link" data-toggle="tab">
                                        <i class="icon-calendar3"></i>
                                        History
                                        <span class="badge bg-success badge-pill ml-auto">
                                            {{ count($data->transactions) }}
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#wishlist" class="nav-link" data-toggle="tab">
                                        <i class="icon-heart5"></i>
                                        Wishlist
                                        @if (count($data->wishlists) > 0)
                                            <span class="badge bg-success badge-pill ml-auto">
                                                {{ count($data->wishlists) }}
                                            </span>
                                        @endif
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                <a href="#review" class="nav-link" data-toggle="tab">
                                    <i class="icon-star-full2"></i>
                                    Review
                                    @if (count($data->productReviews) > 0)
                                    <span class="badge bg-success badge-pill ml-auto mr-1" title="Product Reviews">
                                        {{ count($data->productReviews) }}
                                    </span>
                                    @endif
                                    @if (count($data->merchantReviews) > 0)
                                    <span class="badge bg-info badge-pill" title="Merchant Reviews">
                                        {{ count($data->merchantReviews) }}
                                    </span>
                                    @endif
                                </a>
                            </li> --}}
                                <li class="nav-item-divider"></li>
                                <li class="nav-item">
                                    <a href="#setting" class="nav-link" data-toggle="tab">
                                        <i class="icon-gear"></i>
                                        Account Setting
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /navigation -->
                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /left sidebar component -->


            <!-- Right content -->
            <div class="tab-content w-100">
                <div class="tab-pane fade active show" id="cart">
                    <div class="row">
                        @foreach ($data->carts as $item)
                            <div class="col-md-3 col-sm-3">
                                <div class="card">
                                    <div class="card-img-actions mx-1 mt-1">
                                        <a href="#course_preview" data-toggle="modal">
                                            @if ($item->image)
                                                <img src="{{ asset('/global_assets/images/product/' . $item->image) }}"
                                                    class="img-fluid card-img" alt="{{ $item->name }}"
                                                    style="max-height: 250px;">
                                            @else
                                                <img src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}"
                                                    class="img-fluid card-img" alt="" style="max-height: 250px;">
                                            @endif
                                        </a>
                                    </div>

                                    <div class="card-body">
                                        <div class="mb-3">
                                            <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                                                <a href="{{ route('member.product.show', ['id' => $item->id]) }}"
                                                    class="text-default mr-2">{{ $item->name }}</a>
                                                @if ($item->discount_price)
                                                    <span class="text-warning ml-auto">
                                                        IDR {{ $item->currentPrice() }}
                                                    </span>
                                                @else
                                                    <span class="text-success ml-auto">
                                                        IDR {{ $item->sellPrice() }}
                                                    </span>
                                                @endif
                                            </h6>

                                            <ul class="list-inline list-inline-dotted text-muted mb-0">
                                                <li class="list-inline-item">By <a href="#" class="text-muted">Eugene
                                                        Kopyov</a></li>
                                                <li class="list-inline-item">Nov 1st, 2016</li>
                                            </ul>
                                        </div>

                                        {{ $item->shortDesc() }} <a
                                            href="{{ route('member.product.show', ['id' => $item->id]) }}">[...]</a>
                                    </div>

                                    <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                        <ul class="list-inline list-inline-dotted mb-0">
                                            <li class="list-inline-item">
                                                <a href="{{ route('member.checkout', ['id' => Auth::user()->member->id]) }}"
                                                    class="btn bg-teal-400"><i class="icon-coin-dollar mr-0"></i>
                                                    Checkout</a>
                                            </li>
                                            {{-- <li class="list-inline-item"><i class="icon-alarm mr-2"></i> 60 hours</li> --}}
                                        </ul>
                                        <div class="mt-2 mt-sm-0">
                                            @php
                                                $member = \App\Models\Member::findOrfail(Auth::user()->member->id);
                                                $w = \App\Models\Wishlist::where(['member_id' => $member->id, 'product_id' => $item->id])->first();
                                            @endphp
                                            <input type="hidden" name="mid" id="mid" value="{{ $member->id }}">
                                            <i id="like" class="icon-heart5 text-{{ $w ? 'danger' : 'default' }}"
                                                style="cursor: pointer;" onclick="return likex(this, {{ $item->id }});"
                                                title="Wishlist"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="tab-pane fade" id="history">

                    <!-- Orders history -->
                    <div class="card">

                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Amount</th>
                                        <th>Points</th>
                                        <th>Payment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data->transactions->sortByDesc('created_at') as $trx)
                                        <tr>
                                            <td>
                                                <a href="#" class="font-weight-semibold">{{ $trx->code }}</a>
                                                <div class="text-muted font-size-sm">
                                                    <span class="badge badge-mark bg-grey border-grey mr-1"></span>
                                                    {{ $trx->merchant->name ?? '-' }}
                                                </div>
                                            </td>
                                            <td>
                                                <h6 class="mb-0 font-weight-semibold">IDR {{ $trx->totalAmount() }}</h6>
                                            </td>
                                            <td>{{ $trx->point_earned }}</td>
                                            <td>{{ $trx->paymentType() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /orders history -->

                </div>

                <div class="tab-pane fade" id="wishlist">
                    <div class="row">
                        @foreach ($data->wishlists as $item)
                            <div class="col-md-3 col-sm-3">
                                <div class="card">
                                    <div class="card-img-actions mx-1 mt-1">
                                        <a href="#course_preview" data-toggle="modal">
                                            @if ($item->image)
                                                <img src="{{ asset('/global_assets/images/product/' . $item->image) }}"
                                                    class="img-fluid card-img" alt="{{ $item->name }}"
                                                    style="max-height: 250px;">
                                            @else
                                                <img src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}"
                                                    class="img-fluid card-img" alt="" style="max-height: 250px;">
                                            @endif
                                        </a>
                                    </div>

                                    <div class="card-body">
                                        <div class="mb-3">
                                            <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                                                <a href="{{ route('member.product.show', ['id' => $item->id]) }}"
                                                    class="text-default mr-2">{{ $item->name }}</a>
                                                @if ($item->discount_price)
                                                    <span class="text-warning ml-auto">
                                                        IDR {{ $item->currentPrice() }}
                                                    </span>
                                                @else
                                                    <span class="text-success ml-auto">
                                                        IDR {{ $item->sellPrice() }}
                                                    </span>
                                                @endif
                                            </h6>

                                            <ul class="list-inline list-inline-dotted text-muted mb-0">
                                                <li class="list-inline-item">By <a href="#" class="text-muted">Eugene
                                                        Kopyov</a></li>
                                                <li class="list-inline-item">Nov 1st, 2016</li>
                                            </ul>
                                        </div>

                                        {{ $item->shortDesc() }} <a
                                            href="{{ route('member.product.show', ['id' => $item->id]) }}">[...]</a>
                                    </div>

                                    <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                        <ul class="list-inline list-inline-dotted mb-0">
                                            <li class="list-inline-item"><i class="icon-basket mr-0"></i>
                                                {{ count($item->itemSold) }} Sold
                                            </li>
                                            {{-- <li class="list-inline-item"><i class="icon-alarm mr-2"></i> 60 hours</li> --}}
                                        </ul>

                                        <div class="mt-2 mt-sm-0">
                                            @php
                                                $member = Auth::user()->member;
                                                $w = \App\Models\Wishlist::where(['member_id' => $member->id, 'product_id' => $item->id])->first();
                                            @endphp
                                            <input type="hidden" name="mid" id="mid" value="{{ $member->id }}">
                                            <i id="like" class="icon-heart5 text-{{ $w ? 'danger' : 'default' }}"
                                                style="cursor: pointer;" onclick="return likex(this, {{ $item->id }});"
                                                title="Wishlist"></i>
                                        </div>
                                        <div class="mt-2 mt-sm-0">
                                            <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                            <span class="text-muted ml-1">(49)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                {{-- <div class="tab-pane fade" id="review">

                <!-- Orders history -->
                <div class="card">
                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <thead>
                                <tr>
                                    <th colspan="2">Name</th>
                                    <th>Review</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="table-active">
                                    <td colspan="7" class="font-weight-semibold">Product Review</td>
                                    <td class="text-right">
                                        <span class="badge bg-success badge-pill">
                                            {{ count($data->productReviews) }}
                                        </span>
                                    </td>
                                </tr>

                                @foreach ($data->productReviews as $product)
                                <tr>
                                    <td class="pr-0" style="width: 45px;">
                                        <a href="#">
                                            <img src="{{ asset('/global_assets/images/product/'.$product->image) }}" height="60" alt="">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('member.product.show', ['id'=>$product->id]) }}" class="font-weight-semibold">{{ $product->name }}</a>
                                    </td>
                                    <td>
                                        <i class="icon-star-full2"></i>
                                        {{ $product->pivot->review }}
                                    </td>
                                </tr>
                                @endforeach

                                <tr class="table-active">
                                    <td colspan="7" class="font-weight-semibold">Merchant Review</td>
                                    <td class="text-right">
                                        <span class="badge bg-info badge-pill">
                                            {{ count($data->merchantReviews) }}
                                        </span>
                                    </td>
                                </tr>

                                @foreach ($data->merchantReviews as $merchant)
                                <tr>
                                    <td class="pr-0">
                                        <a href="#">
                                            <img src="{{ asset('/global_assets/images/merchant/'. $merchant->photo) }}" height="60" alt="">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="font-weight-semibold">{{ $merchant->name }}</a>
                                    </td>
                                    <td>
                                        <i class="icon-star-full2"></i>
                                        {{ $merchant->pivot->review }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /orders history -->

            </div> --}}

                <div class="tab-pane fade" id="setting">

                    <!-- Account settings -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Account settings</h5>
                        </div>

                        <div class="card-body">
                            <form action="{{ route('member.profile.update', ['id' => $data->id]) }}" method="POST">
                                @method('POST')
                                @csrf
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Name*</label>
                                            <input type="text" name="name" value="{{ $data->name }}" class="form-control"
                                                autocomplete="off" required>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Email*</label>
                                            <input type="email" name="email" value="{{ $data->email }}"
                                                class="form-control" autocomplete="off" required>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Phone</label>
                                            <input type="tel" name="phone" value="{{ $data->phone }}"
                                                class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>New password</label>
                                            <input type="password" name="password" placeholder="Enter new password"
                                                class="form-control" autocomplete="off">
                                        </div>

                                        <div class="col-md-6">
                                            <label>Repeat password</label>
                                            <input type="password" name="passowd_confirmation"
                                                placeholder="Repeat new password" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /account settings -->

                </div>
            </div>
            <!-- /right content -->

        </div>
        <!-- /inner container -->

    </div>
@endsection
@push('additional_js_plugin')
    {{-- Checkbox --}}
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/learning.js') }}"></script>
    {{-- Select --}}
    <script src="{{ asset('/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- Animation --}}
    <script src="{{ asset('/global_assets/js/demo_pages/animations_css3.js') }}"></script>
    {{-- Sweet Alert --}}
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js', []) }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-profile').addClass('active');
        });

        function likex(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';

            if (like.hasClass('text-default')) {
                optx = 'like';
            } else if (like.hasClass('text-danger')) {
                optx = 'unlike';
            }

            $.ajax({
                    type: "POST",
                    url: "{{ url('member/like') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('text-default')) {
                        like.removeClass('text-default');
                        like.addClass('text-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to wishlist!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('text-danger')) {
                        like.removeClass('text-danger');
                        like.addClass('text-default');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from wishlist!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                    window.location.reload();
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

    </script>
@endpush
