@extends('template.member_template')
@push('additional_css_plugin')
    <style>
        .rating {
            display: flex;
            flex-direction: row-reverse;
            justify-content: center
        }

        .rating>input {
            display: none
        }

        .rating>label {
            position: relative;
            width: 1em;
            font-size: 35px;
            color: #f44336;
            cursor: pointer
        }

        .rating>label::before {
            content: "\2605";
            position: absolute;
            opacity: 0
        }

        .rating>label:hover:before,
        .rating>label:hover~label:before {
            opacity: 1 !important
        }

        .rating>input:checked~label:before {
            opacity: 1
        }

        .rating:hover>input:checked~label:before {
            opacity: 0.4
        }

        h1,
        p {
            text-align: center
        }

        h1 {
            margin-top: 150px
        }

        p {
            font-size: 1.2rem
        }

        @media only screen and (max-width: 600px) {
            h1 {
                font-size: 14px
            }

            p {
                font-size: 12px
            }
        }

    </style>
@endpush
@push('second_header')
    <div class="page-title d-flex">
        <h4>
            <a href="{{ url()->previous() }}" class="text-default"><i class="icon-arrow-left52 mr-2"></i></a>
            <span class="font-weight-semibold">{{ $data->name }}</span>
        </h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>

    {{-- <div class="header-elements d-none py-0 mb-3 mb-md-0">
    <div class="breadcrumb">
        <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Merhcant</a>
        <span class="breadcrumb-item active">{{ $data->name }}</span>
    </div>
</div> --}}
@endpush
@section('content')
    <div class="content">

        <!-- Inner container -->
        <div class="d-flex align-items-start flex-column flex-md-row">

            <!-- Left content -->
            <div class="w-100 overflow-auto order-2 order-md-1">

                <!-- Post -->
                <div class="card">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="mb-3 text-center">
                                @if ($data->image)
                                    <a href="#" class="d-inline-block">
                                        <img src="{{ asset('/global_assets/images/product/' . $data->image) }}"
                                            class="img-fluid" alt="">
                                    </a>
                                @else
                                    <a href="#" class="d-inline-block">
                                        <img src="/global_assets/images/placeholders/cover.jpg" class="img-fluid" alt="">
                                    </a>
                                @endif
                            </div>

                            <h4 class="font-weight-semibold mb-1">
                                <a href="#" class="text-default">{{ $data->name }}</a>
                            </h4>

                            <ul class="list-inline list-inline-dotted text-muted mb-3">
                                <li class="list-inline-item">By <a href="#" class="text-muted">Eugene</a></li>
                                <li class="list-inline-item">July 5th, 2016</li>
                                <li class="list-inline-item"><a href="#" class="text-muted">12 comments</a></li>
                                <li class="list-inline-item"><a href="#" class="text-muted"><i
                                            class="icon-heart6 font-size-base text-pink mr-2"></i> 281</a></li>
                            </ul>

                            <div class="mb-3">
                                <p>
                                    {{ $data->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /post -->


                <!-- About author -->
                <div class="card">
                    <div class="media card-body flex-column flex-md-row m-0">
                        <div class="mr-md-3 mb-2 mb-md-0">
                            @if ($data->merchant->photo)
                                <a href="{{ route('member.merchant.show', ['id' => $data->merchant->id]) }}">
                                    <img src="{{ asset('/global_assets/images/merchant/' . $data->merchant->photo) }}"
                                        class="rounded-circle" width="64" height="64" alt="">
                                </a>
                            @else
                                <a href="{{ route('member.merchant.show', ['id' => $data->merchant->id]) }}">
                                    <img src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}"
                                        class="rounded-circle" width="64" height="64" alt="">
                                </a>
                            @endif
                        </div>

                        <div class="media-body">
                            <h6 class="media-title font-weight-semibold mt-3">
                                <a href="{{ route('member.merchant.show', ['id' => $data->merchant->id]) }}">
                                    {{ $data->merchant->name }}
                                </a>
                            </h6>
                            {{-- <p>
                            Check another product from this merchant
                        </p> --}}

                            {{-- <ul class="list-inline list-inline-dotted mb-0">
                            <li class="list-inline-item"><a href="#">More product</a></li>
                            <li class="list-inline-item"><a href="#">All posts by James</a></li>
                            <li class="list-inline-item"><a href="#">http://website.com</a></li>
                        </ul> --}}
                        </div>
                    </div>
                </div>
                <!-- /about author -->

            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            <div
                class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- Categories -->
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="card-title font-weight-semibold">Description</span>
                            {{-- <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div> --}}
                        </div>

                        <div class="card-body p-0">
                            <div class="nav nav-sidebar my-2">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="icon-coin-dollar"></i>
                                        IDR {{ $data->currentPrice() }}
                                        {{-- <span class="text-muted font-size-sm font-weight-normal ml-auto">12</span> --}}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="icon-price-tag2"></i>
                                        {{ $data->type() }}
                                        {{-- <span class="text-muted font-size-sm font-weight-normal ml-auto">26</span> --}}
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-star-full2"></i>
                                    {{ $data->review }}
                                    <span class="text-muted font-size-sm font-weight-normal ml-auto">83</span>
                                </a>
                            </li> --}}
                            </div>
                        </div>
                    </div>
                    <!-- /categories -->

                    <!-- Options -->
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="card-title font-weight-semibold">Purchase Options</span>
                        </div>


                        <div class="card-body pb-0">
                            <ul class="list-inline list-inline-condensed text-center mb-0">
                                <li class="list-inline-item">
                                    @php
                                        $member = Auth::user()->member->id;
                                        $w = \App\Models\Wishlist::where(['member_id' => $member, 'product_id' => $data->id])->first();
                                        $y = \App\Models\Cart::where(['member_id' => $member, 'product_id' => $data->id])->first();
                                    @endphp
                                    <input type="hidden" name="mid" id="mid" value="{{ $member }}">
                                    <a id="like"
                                        class="btn bg-{{ $w ? 'danger' : 'indigo' }} btn-icon btn-lg rounded-round mb-3"
                                        style="cursor: pointer;" onclick="return likex(this, {{ $data->id }});"
                                        title="Wishlist">
                                        <i class="icon-heart5"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a id="cart"
                                        class="btn bg-{{ $y ? 'danger' : 'indigo' }} btn-icon btn-lg rounded-round mb-3"
                                        style="cursor: pointer;" onclick="return cartx(this, {{ $data->id }});"
                                        title="Add to cart">
                                        <i class="icon-basket"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /Options -->

                    <!-- Share -->
                    {{-- <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">Your Rating</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    

                    <div class="card-body pb-0">
                        <ul class="list-inline list-inline-condensed text-center mb-0">
                            <li class="list-inline-item">
                                <div class="rating">
                                    <input type="radio" name="rating" value="5" id="5" 
                                    @if ($review)
                                        {{ $review->review == 5 ? 'checked' : '' }}
                                    @endif
                                    >
                                        <label for="5">☆</label>
                                    <input type="radio" name="rating" value="4" id="4" 
                                    @if ($review)
                                        {{ $review->review == 4 ? 'checked' : '' }}
                                    @endif
                                    >
                                        <label for="4">☆</label>
                                    <input type="radio" name="rating" value="3" id="3"
                                    @if ($review)
                                        {{ $review->review == 3 ? 'checked' : '' }}
                                    @endif
                                    >
                                        <label for="3">☆</label>
                                    <input type="radio" name="rating" value="2" id="2"
                                    @if ($review)
                                        {{ $review->review == 2 ? 'checked' : '' }}
                                    @endif
                                    >
                                        <label for="2">☆</label>
                                    <input type="radio" name="rating" value="1" id="1"
                                    @if ($review)
                                        {{ $review->review == 1 ? 'checked' : '' }}
                                    @endif
                                    >
                                        <label for="1">☆</label>
                                </div>
                            </li>
                           @if ($review == null)
                           <button type="submit" class="btn btn-danger rounded-round mb-4" onclick="return review({{ request()->id }});">
                                <i class="icon-pushpin mr-2"></i> Submit
                            </button>
                           @endif
                        </ul>
                    </div>
                </div> --}}
                    <!-- /share -->

                    <!-- Share -->
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="card-title font-weight-semibold">Share</span>
                            {{-- <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div> --}}
                        </div>


                        <div class="card-body pb-0">
                            <ul class="list-inline list-inline-condensed text-center mb-0">
                                <li class="list-inline-item">
                                    <a href="#" class="btn bg-indigo btn-icon btn-lg rounded-round mb-3">
                                        <i class="icon-facebook"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn bg-danger btn-icon btn-lg rounded-round mb-3">
                                        <i class="icon-youtube3"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn bg-info btn-icon btn-lg rounded-round mb-3">
                                        <i class="icon-twitter"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn bg-orange btn-icon btn-lg rounded-round mb-3">
                                        <i class="icon-feed3"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /share -->

                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
@endsection
@push('additional_js_plugin')
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/learning.js') }}"></script>
    {{-- Sweet Alert --}}
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js', []) }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-product').addClass('active');
        });

        function likex(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';

            if (like.hasClass('bg-indigo')) {
                optx = 'like';
            } else if (like.hasClass('bg-danger')) {
                optx = 'unlike';
            }

            $.ajax({
                    type: "POST",
                    url: "{{ url('member/like') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('bg-indigo')) {
                        like.removeClass('bg-indigo');
                        like.addClass('bg-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to wishlist!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('bg-danger')) {
                        like.removeClass('bg-danger');
                        like.addClass('bg-indigo');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from wishlist!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

        function cartx(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';
            if (like.hasClass('bg-indigo')) {
                optx = 'add';
            } else if (like.hasClass('bg-danger')) {
                optx = 'remove';
            }

            $.ajax({
                    type: "POST",
                    url: "{{ url('member/cart') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('bg-indigo')) {
                        like.removeClass('bg-indigo');
                        like.addClass('bg-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to cart!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('bg-danger')) {
                        like.removeClass('bg-danger');
                        like.addClass('bg-indigo');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from cart!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

        function review(pid) {
            var uid = '{{ Auth::user()->id }}';
            var pid = pid;
            var stars = $('input[name="rating"]:checked').val();

            swal({
                    title: "Submit review?",
                    icon: 'info',
                    buttons: true,
                    dangerMode: false
                })
                .then((confirmx) => {
                    if (confirmx) {

                        $.ajax({
                                type: "POST",
                                url: "{{ url('member/reviewProduct') }}",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    pid: pid,
                                    uid: uid,
                                    stars: stars
                                },
                                dataType: "json"
                            })
                            .done(function(data) {
                                swal("Thank you for submitting review.", {
                                    icon: 'success'
                                });
                                console.log(data);
                            })
                            .fail(function() {
                                swal({
                                    title: "Opss!",
                                    text: "An error acquired!",
                                    icon: "error",
                                    button: 'Close'
                                });
                            });
                    } else {
                        swal("Review cancelled");
                    }
                });
        }

    </script>
@endpush
