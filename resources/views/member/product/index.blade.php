@extends('template.member_template')
@push('additional_css_plugin')

@endpush
@push('second_header')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>

    <div class="header-elements d-none py-0 mb-3 mb-md-0">
        <div class="breadcrumb">
            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </div>
    </div>
@endpush
@section('content')
    <div class="content">

        <!-- Inner container -->
        <div class="d-flex align-items-start flex-column flex-md-row">

            <!-- Left content -->
            <div class="w-100 order-2 order-md-1">

                <!-- Vertical cards -->
                <div class="row">
                    @foreach ($data as $item)
                        <div class="col-md-3 col-sm-3">
                            <div class="card">
                                <div class="card-img-actions mx-1 mt-1">
                                    <a href="#course_preview" data-toggle="modal">
                                        @if ($item->image)
                                            <img src="{{ asset('/global_assets/images/product/' . $item->image) }}"
                                                class="img-fluid card-img" alt="{{ $item->name }}"
                                                style="max-height: 250px;">
                                        @else
                                            <img src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}"
                                                class="img-fluid card-img" alt="" style="max-height: 250px;">
                                        @endif
                                    </a>
                                </div>

                                <div class="card-body">
                                    <div class="mb-3">
                                        <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                                            <a href="{{ route('member.product.show', ['id' => $item->id]) }}"
                                                class="text-default mr-2">{{ $item->name }}</a>
                                            @if ($item->discount_price)
                                                <span class="text-warning ml-auto">
                                                    IDR {{ $item->currentPrice() }}
                                                </span>
                                            @else
                                                <span class="text-success ml-auto">
                                                    IDR {{ $item->sellPrice() }}
                                                </span>
                                            @endif
                                        </h6>

                                        <ul class="list-inline list-inline-dotted text-muted mb-0">
                                            <li class="list-inline-item">{{ $item->type() }}</li>
                                            {{-- <li class="list-inline-item">Nov 1st, 2016</li> --}}
                                        </ul>
                                    </div>

                                    {{ $item->shortDesc() }} <a
                                        href="{{ route('member.product.show', ['id' => $item->id]) }}">[...]</a>
                                </div>

                                <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                    <ul class="list-inline list-inline-dotted mb-0">
                                        <li class="list-inline-item"><i class="icon-basket mr-0"></i>
                                            {{ count($item->itemSold) }} Sold
                                        </li>
                                        {{-- <li class="list-inline-item"><i class="icon-alarm mr-2"></i> 60 hours</li> --}}
                                    </ul>

                                    <div class="mt-2 mt-sm-0">
                                        @php
                                            $mid = Auth::user()->member->id;
                                            $w = \App\Models\Wishlist::where(['member_id' => $mid, 'product_id' => $item->id])->first();
                                        @endphp
                                        <input type="hidden" name="mid" id="mid" value="{{ $mid }}">
                                        <i id="like" class="icon-heart5 text-{{ $w ? 'danger' : 'default' }}"
                                            style="cursor: pointer;" onclick="return likex(this, {{ $item->id }});"
                                            title="Wishlist"></i>
                                    </div>
                                    {{-- <div class="mt-2 mt-sm-0">
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted ml-1">({{ $item->reviews() }})</span>
                            </div> --}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- /vertical cards -->


                <!-- Pagination -->
                <div class="d-flex justify-content-center mt-3 mb-3">
                    <ul class="pagination">
                        <li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-left12"></i></a></li>
                        <li class="page-item active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">3</a></li>
                        <li class="page-item"><a href="#" class="page-link">4</a></li>
                        <li class="page-item"><a href="#" class="page-link">5</a></li>
                        <li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-right13"></i></a></li>
                    </ul>
                </div>
                <!-- /pagination -->

            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            <div
                class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- Find course -->
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">Filter</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <form action="{{ route('member.product', []) }}">
                                {{-- <input type="hidden" name="s" id="" value="1"> --}}
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="discounted">Type Options</label>
                                        <select class="form-control form-control-uniform" name="type">
                                            <option value="" selected hidden>Type</option>
                                            <option value="0" {{ request()->type == 0 ? 'selected' : '' }}> All</option>
                                            <option value="2" {{ request()->type == 2 ? 'selected' : '' }}> Food</option>
                                            <option value="1" {{ request()->type == 1 ? 'selected' : '' }}> Beverage
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="discounted">Discount Options</label>
                                        <select class="form-control form-control-uniform" name="discounted">
                                            <option value="" selected hidden>Discount Only</option>
                                            <option value="1" {{ request()->discounted == 1 ? 'selected' : '' }}> Yes
                                            </option>
                                            <option value="0" {{ request()->discounted == 0 ? 'selected' : '' }}> No
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                <div class="form-group">
                                    <select class="form-control form-control-uniform" name="review">
                                        <option value="" selected hidden>Review</option>
                                        <option value="1" {{ request()->review == 1 ? 'selected' : ''}}> > 3 Stars</option>
                                        <option value="0" {{ request()->review == 0 ? 'selected' : ''}}> < 3 Stars</option>
                                    </select>
                                </div>
                            </div> --}}
                                @if (request()->type || request()->revidiscountedew || request()->review)
                                    <a href="{{ route('member.product', []) }}" class="btn bg-warning btn-block">
                                        <i class="icon-blocked mr-2"></i>
                                        Clear Filter
                                    </a>
                                @endif
                                <button type="submit" class="btn bg-blue btn-block">
                                    <i class="icon-filter4 mr-2"></i>
                                    Filter
                                </button>
                            </form>
                        </div>
                    </div>
                    <!-- /find course -->

                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
@endsection
@push('additional_js_plugin')
    {{-- Checkbox --}}
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/learning.js') }}"></script>
    {{-- Select --}}
    <script src="{{ asset('/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- Sweet Alert --}}
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js', []) }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-product').addClass('active');
        });

        function likex(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';

            if (like.hasClass('text-default')) {
                optx = 'like';
            } else if (like.hasClass('text-danger')) {
                optx = 'unlike';
            }

            $.ajax({
                    type: "POST",
                    url: "{{ url('member/like') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('text-default')) {
                        like.removeClass('text-default');
                        like.addClass('text-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to wishlist!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('text-danger')) {
                        like.removeClass('text-danger');
                        like.addClass('text-default');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from wishlist!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

    </script>
@endpush
