@extends('template.member_template')
@push('additional_css_plugin')

@endpush
@push('second_header')
    <div class="page-title d-flex">
        <h4>
            <a href="{{ url()->previous() }}" class="text-default"><i class="icon-arrow-left52 mr-2"></i></a>
            <span class="font-weight-semibold">{{ $data->name }}</span>
        </h4>
        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>

    {{-- <div class="header-elements d-none py-0 mb-3 mb-md-0">
    <div class="breadcrumb">
        <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Merhcant</a>
        <span class="breadcrumb-item active">{{ $data->name }}</span>
    </div>
</div> --}}
@endpush
@section('content')
    <div class="content">

        <!-- Inner container -->
        <div class="d-md-flex align-items-md-start">

            <!-- Left sidebar component -->
            <div
                class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-left wmin-300 border-0 shadow-0 sidebar-expand-md">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <!-- Navigation -->
                    <div class="card">
                        <div class="card-body bg-indigo-400 text-center card-img-top" style="background-image: url("
                            {{ asset('/global_assets/images/backgrounds/panel_bg.png') }}"); background-size: contain;">
                            <div class="card-img-actions d-inline-block mb-3">
                                @if ($data->photo)
                                    <img class="img-fluid rounded-circle"
                                        src="{{ asset('/global_assets/images/merchant/' . $data->photo) }}" width="170"
                                        height="170" alt="">
                                @else
                                    <img class="img-fluid rounded-circle"
                                        src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}"
                                        width="170" height="170" alt="">
                                @endif
                                {{-- <div class="card-img-actions-overlay rounded-circle">
                                <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
                                    <i class="icon-plus3"></i>
                                </a>
                                <a href="user_pages_profile.html" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                    <i class="icon-link"></i>
                                </a>
                            </div> --}}
                            </div>

                            <h6 class="font-weight-semibold mb-0">{{ $data->name }}</h6>
                            {{-- <span class="d-block opacity-75">
                            <i class="icon-star-full2">
                                {{ $data->review }}
                            </i>
                        </span> --}}

                            {{-- <div class="list-icons list-icons-extended mt-3">
                            <a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Google Drive"><i class="icon-google-drive"></i></a>
                            <a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Twitter"><i class="icon-twitter"></i></a>
                            <a href="#" class="list-icons-item text-white" data-popup="tooltip" title="" data-container="body" data-original-title="Github"><i class="icon-github"></i></a>
                        </div> --}}
                        </div>

                        <div class="card-body p-0">
                            <ul class="nav nav-sidebar mb-2">
                                {{-- <li class="nav-item-header">Navigation</li> --}}
                                <li class="nav-item">
                                    <a href="#product" class="nav-link active" data-toggle="tab">
                                        <i class="fa fa-utensils"></i>
                                        Products
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                <a href="#review" class="nav-link" data-toggle="tab">
                                    <i class="icon-star-full2"></i>
                                    Reviews
                                </a>
                            </li> --}}
                            </ul>
                        </div>
                    </div>
                    <!-- /navigation -->

                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /left sidebar component -->


            <!-- Right content -->
            <div class="tab-content w-100">
                <div class="tab-pane fade active show" id="product">

                    <!-- Invoices -->
                    <div class="row">
                        @foreach ($data->products as $product)
                            <div class="col-xl-3 col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-img-actions">
                                            @if ($product->image)
                                                <a href="{{ asset('/global_assets/images/product/' . $product->image) }}"
                                                    data-popup="lightbox">
                                                    <img src="{{ asset('/global_assets/images/product/' . $product->image) }}"
                                                        class="card-img" width="96" alt="">
                                                    <span class="card-img-actions-overlay card-img">
                                                        <i class="icon-plus3 icon-2x"></i>
                                                    </span>
                                                </a>
                                            @else
                                                <a href="/global_assets/images/placeholders/placeholder.jpg"
                                                    data-popup="lightbox">
                                                    <img src="/global_assets/images/placeholders/placeholder.jpg"
                                                        class="card-img" width="96" alt="">
                                                    <span class="card-img-actions-overlay card-img">
                                                        <i class="icon-plus3 icon-2x"></i>
                                                    </span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="card-body bg-light text-center">
                                        <div class="mb-2">
                                            <h6 class="font-weight-semibold mb-0">
                                                <a href="{{ route('member.product.show', ['id' => $product->id]) }}"
                                                    class="text-default">{{ $product->name }}</a>
                                            </h6>

                                            <a href="#" class="text-muted">{{ $product->type() }}</a>
                                        </div>

                                        <h3 class="mb-0 font-weight-semibold">{{ $product->currentPrice() }}</h3>

                                        {{-- <div>
                                    <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                    <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                </div>

                                <div class="text-muted mb-3">{{ $product->review }} reviews</div> --}}
                                        @php
                                            $member = Auth::user()->member->id;
                                            $y = \App\Models\Cart::where(['member_id' => $member, 'product_id' => $data->id])->first();
                                        @endphp
                                        <input type="hidden" name="mid" id="mid" value="{{ $member }}">
                                        {{-- <button type="button" class="btn bg-teal-400"><i class="icon-heart5"></i></button> --}}
                                        {{-- <a class="btn bg-teal-400 bg-{{ $y ? 'danger' : 'indigo' }}" style="cursor: pointer;" onclick="return cartx(this, {{ $data->id }});" title="Add to cart">
                                    <i class="icon-cart-add mr-2"></i> Add to cart
                                </a> --}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- /invoices -->

                </div>

                <div class="tab-pane fade" id="review">

                    <div class="row">
                        @foreach ($data->reviews as $review)
                            <div class="col-lg-6">
                                <div class="card border-left-3 border-left-danger rounded-left-0">
                                    <div class="card-body">
                                        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                                            <p>
                                                {{ $review->comment }}
                                            </p>
                                        </div>
                                    </div>

                                    <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                        <span>
                                            <span class="badge badge-mark border-danger mr-2"></span>
                                            By:
                                            <span class="font-weight-semibold">{{ $review->member->name }}</span>
                                        </span>

                                        <ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
                                            <span class="badge badge-mark border-danger mr-2"></span>
                                            <span class="font-weight-semibold">{{ $review->createdDate() }}</span>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
            <!-- /right content -->

        </div>
        <!-- /inner container -->

    </div>
@endsection
@push('additional_js_plugin')
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/learning.js') }}"></script>
    {{-- Sweet Alert --}}
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js', []) }}"></script>
    {{-- Fancy Box --}}
    <script src="{{ asset('/global_assets/js/plugins/media/fancybox.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/ecommerce_product_list.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-merchant').addClass('active');
        });

        function likex(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';

            if (like.hasClass('text-default')) {
                optx = 'like';
            } else if (like.hasClass('text-danger')) {
                optx = 'unlike';
            }

            $.ajax({
                    type: "POST",
                    url: "member/like",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('text-default')) {
                        like.removeClass('text-default');
                        like.addClass('text-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to wishlist!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('text-danger')) {
                        like.removeClass('text-danger');
                        like.addClass('text-default');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from wishlist!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

        function cartx(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';
            if (like.hasClass('bg-indigo')) {
                optx = 'add';
            } else if (like.hasClass('bg-danger')) {
                optx = 'remove';
            }

            $.ajax({
                    type: "POST",
                    url: "{{ url('member/cart') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('bg-indigo')) {
                        like.removeClass('bg-indigo');
                        like.addClass('bg-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to cart!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('bg-danger')) {
                        like.removeClass('bg-danger');
                        like.addClass('bg-indigo');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from cart!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

    </script>
@endpush
