@extends('template.member_template')
@push('additional_css_plugin')

@endpush
@push('second_header')
<div class="page-title d-flex">
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>

<div class="header-elements d-none py-0 mb-3 mb-md-0">
    <div class="breadcrumb">
        <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item active">Dashboard</span>
    </div>
</div>
@endpush
@section('content')
<div class="content">

    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="w-100 order-2 order-md-1">

            <!-- Vertical cards -->
            <div class="row">
                @foreach ($data as $item)
                <div class="col-md-3 col-sm-3">
                    <div class="card">
                        <div class="card-img-actions mx-1 mt-1">
                            <a href="#course_preview" data-toggle="modal">
                                @if ($item->photo)
                                    <img src="{{ asset('/global_assets/images/merchant/'. $item->photo) }}" class="img-fluid card-img" alt="" style="max-height: 180px;">
                                @else
                                    <img src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}" class="img-fluid card-img" alt="">
                                @endif
                            </a>
                        </div>

                        <div class="card-body">
                            <div class="mb-0">
                                <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                                    <a href="{{ route('member.merchant.show', ['id' => $item->id]) }}" class="text-default mr-2">
                                        <i class="icon-shield-check" style="color: #41847b"></i>
                                        {{ $item->name }}
                                    </a>
                                    <span class="text-success ml-auto">{{ $item->code }}</span>
                                </h6>

                                <ul class="list-inline list-inline-dotted text-muted mb-0">
                                    <li class="list-inline-item"><span class="text-{{ $item->is_open == 1 ? 'success' : 'danger' }} ml-auto">{{ $item->is_open == 1 ? 'Open' : 'Close' }}</span></li>
                                    <li class="list-inline-item">By <a href="#" class="text-muted">{{ $item->email }}</a></li>
                                    <li class="list-inline-item">{{ $item->phone }}</li>
                                </ul>
                            </div>

                            {{-- One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed <a href="#">[...]</a> --}}
                        </div>

                        {{-- <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">

                            <div class="mt-2 mt-sm-0">
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted ml-1">({{ $item->review }})</span>
                            </div>
                        </div> --}}
                    </div>
                </div>
                @endforeach
            </div>
            <!-- /vertical cards -->


            <!-- Pagination -->
            <div class="d-flex justify-content-center mt-3 mb-3">
                <ul class="pagination">
                    <li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-left12"></i></a></li>
                    <li class="page-item active"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                    <li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-right13"></i></a></li>
                </ul>
            </div>
            <!-- /pagination -->

        </div>
        <!-- /left content -->


        <!-- Right sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- Find course -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Filter</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form class="mb-3" action="{{ route('member.merchant', []) }}">
                            <input type="hidden" name="s" id="" value="1">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" value="{{ request()->name ?? ''  }}" placeholder="Search by Name" autocomplete="off">
                                        <span class="input-group-append">
                                            <button class="btn btn-light" type="submit">
                                                <i class="icon-search4"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form action="{{ route('member.merchant', []) }}">
                            {{-- <input type="hidden" name="s" id="" value="1"> --}}
                            {{-- <div class="form-group">
                                <div class="form-group">
                                    <select class="form-control form-control-uniform" name="review">
                                        <option value="" selected hidden>Review</option>
                                        <option value="1" {{ request()->review == 1 ? 'selected' : ''}}> > 3 Stars</option>
                                        <option value="0" {{ request()->review == 0 ? 'selected' : ''}}> < 3 Stars</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <div class="form-group">
                                    <select class="form-control form-control-uniform" name="is_open">
                                        <option value="" selected hidden>Open Status</option>
                                        <option value="1" {{ request()->is_open == 1 ? 'selected' : ''}}> Open Only</option>
                                        <option value="0" {{ request()->is_open == 0 ? 'selected' : ''}}> All Merchants</option>
                                    </select>
                                </div>
                            </div>
                            @if (request()->name || request()->review || request()->is_open)
                            <a href="{{ route('member.merchant', []) }}" class="btn bg-warning btn-block">
                                <i class="icon-blocked mr-2"></i>
                                Clear Filter
                            </a>
                            @endif
                            <button type="submit" class="btn bg-blue btn-block">
                                <i class="icon-filter4 mr-2"></i>
                                Filter
                            </button>
                        </form>
                    </div>
                </div>
                <!-- /find course -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /right sidebar component -->

    </div>
    <!-- /inner container -->

</div>
@endsection
@push('additional_js_plugin')
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/learning.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-merchant').addClass('active');
        });
    </script>
@endpush