@extends('template.member_template')
@push('additional_css_plugin')
    {{-- Animation --}}
    <link href="{{ asset('/global_assets/css/extras/animate.min.css') }}" rel="stylesheet" type="text/css">
@endpush
@push('second_header')
    {{-- <div class="page-title d-flex">
    <h4><i class="icon-spotlight2 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div> --}}
@endpush
@section('content')
    <div class="content">

        <!-- Inner container -->
        <div class="d-flex align-items-start flex-column flex-md-row" style="margin-top: -30px;">

            <!-- Left content -->
            <div class="w-100 order-2 order-md-1">
                <div class="page-title d-flex">
                    <h4><i class="icon-spotlight2 mr-2"></i> <span class="font-weight-semibold">Recommendation</span> -
                        Merchant</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                <!-- Vertical cards -->
                <div class="row">
                    @foreach ($merchants as $merchant)
                        <div class="col-md-2 col-sm-3">
                            <div class="card">
                                <div class="card-img-actions mx-1 mt-1">
                                    <a href="#course_preview" data-toggle="modal">
                                        @if ($merchant->photo)
                                            <img src="{{ asset('/global_assets/images/merchant/' . $merchant->photo) }}"
                                                class="img-fluid card-img" style="max-height: 150px;" alt="">
                                        @else
                                            <img src="{{ asset('global_assets/images/placeholders/placeholder.jpg') }}"
                                                class="img-fluid card-img" style="max-height: 150px;" alt="">
                                        @endif
                                    </a>
                                </div>

                                <div class="card-body">
                                    <div class="mb-0">
                                        <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                                            <a href="{{ route('member.merchant.show', ['id' => $merchant->id]) }}"
                                                class="text-default mr-2">
                                                <i class="icon-shield-check" style="color: #41847b"></i>
                                                {{ $merchant->name }}
                                            </a>
                                            <span class="text-info ml-auto">{{ $merchant->code }}</span>
                                        </h6>

                                        <ul class="list-inline list-inline-dotted text-muted mb-0">
                                            <li class="list-inline-item"><span
                                                    class="text-{{ $merchant->is_open == 1 ? 'success' : 'danger' }} ml-auto">{{ $merchant->is_open == 1 ? 'Open' : 'Close' }}</span>
                                            </li>
                                            {{-- <li class="list-inline-item">By <a href="#" class="text-muted">Eugene Kopyov</a></li> --}}
                                            <li class="list-inline-item">{{ $merchant->phone }}</li>
                                            <li class="list-inline-item">{{ $merchant->email }}</li>
                                        </ul>
                                    </div>
                                </div>

                                {{-- <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                            <div class="mt-2 mt-sm-0">
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted ml-1">({{ $merchant->review }})</span>
                            </div>
                        </div> --}}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row" style="z-index: 10;">
                    <div class="col-md-12 text-center mt-2">
                        <a href="{{ route('member.merchant', []) }}"
                            class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i
                                    class="icon-link"></i></b>More merchants</a>
                    </div>
                </div>
                <!-- /vertical cards -->
                <div class="page-title d-flex" style="margin-top: -30px; z-index: -5;">
                    <h4><i class="icon-spotlight2 mr-2"></i> <span class="font-weight-semibold">Recommendation</span> -
                        Product</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                <div class="row">
                    @foreach ($products as $product)
                        <div class="col-md-2 col-sm-3">
                            <div class="card">
                                <div class="card-img-actions mx-1 mt-1">
                                    <a href="#course_preview" data-toggle="modal">
                                        @if ($product->image)
                                            <img src="{{ asset('/global_assets/images/product/' . $product->image) }}"
                                                class="img-fluid card-img" style="max-height: 150px;" alt="">
                                        @else
                                            <img src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}"
                                                class="img-fluid card-img" style="max-height: 150px;" alt="">
                                        @endif
                                    </a>
                                </div>

                                <div class="card-body">
                                    <div class="mb-0">
                                        <h6 class="d-flex font-weight-semibold flex-nowrap mb-0">
                                            <a href="{{ route('member.product.show', ['id' => $product->id]) }}"
                                                class="text-default mr-2">{{ $product->name }}</a>
                                            @if ($product->type == 1)
                                                <span class="text-info ml-auto">
                                                    <i class="fa fa-coffee" style="color: #41847b"></i>
                                                </span>
                                            @else
                                                <span class="text-info ml-auto">
                                                    <i class="fa fa-utensils" style="color: #41847b"></i>
                                                </span>
                                            @endif
                                        </h6>

                                        <ul class="list-inline list-inline-dotted text-muted mb-0">
                                            <li class="list-inline-item">
                                                <span class="badge badge-success">IDR
                                                    {{ $product->currentPrice() }}</span>
                                            </li>
                                            <li class="list-inline-item">{{ $product->merchant->name }}</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                    <div class="mt-2 mt-sm-0">
                                        {{-- <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted">({{ $product->review }})</span> --}}
                                    </div>
                                    <span class="text-info ml-auto">
                                        @php
                                            $member = Auth::user()->member;
                                            $w = \App\Models\Wishlist::where(['member_id' => $member->id, 'product_id' => $product->id])->first();
                                        @endphp
                                        {{-- <input type="hidden" name="pid" id="pid" value="{{ $product->id }}"> --}}
                                        <input type="hidden" name="mid" id="mid" value="{{ $member->id }}">
                                        <i id="like" class="icon-heart5 text-{{ $w ? 'danger' : 'default' }}"
                                            style="cursor: pointer;" onclick="return likex(this, {{ $product->id }});"
                                            title="Wishlist"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- {{ $x ?? '' }} --}}
                </div>
                <div class="row">
                    <div class="col-md-12 text-center mt-2">
                        <a href="{{ route('member.product', []) }}"
                            class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i
                                    class="icon-link"></i></b>More product</a>
                    </div>
                </div>

                <!-- Pagination -->
                {{-- <div class="d-flex justify-content-center mt-3 mb-3">
                <ul class="pagination">
                    <li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-left12"></i></a></li>
                    <li class="page-item active"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                    <li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-right13"></i></a></li>
                </ul>
            </div> --}}
                <!-- /pagination -->

            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            {{-- <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- Find course -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Find course</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form class="mb-3" action="#">
                            <div class="form-group form-group-feedback form-group-feedback-right">
                                <input type="search" class="form-control" placeholder="Type and hit Enter">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 font-size-base text-muted"></i>
                                </div>
                            </div>
                        </form>

                        <div class="form-group">
                            <select class="form-control select select2-hidden-accessible" data-fouc="" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                <optgroup label="Mountain Time Zone" data-select2-id="233">
                                    <option value="AZ" data-select2-id="3">Arizona</option>
                                    <option value="CO" data-select2-id="234">Colorado</option>
                                    <option value="ID" data-select2-id="235">Idaho</option>
                                    <option value="WY" data-select2-id="236">Wyoming</option>
                                </optgroup>
                                <optgroup label="Central Time Zone" data-select2-id="237">
                                    <option value="AL" data-select2-id="238">Alabama</option>
                                    <option value="AR" data-select2-id="239">Arkansas</option>
                                    <option value="KS" data-select2-id="240">Kansas</option>
                                    <option value="KY" data-select2-id="241">Kentucky</option>
                                </optgroup>
                            </select><span class="select2 select2-container select2-container--default select2-container--below select2-container--focus" dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-ga5a-container"><span class="select2-selection__rendered" id="select2-ga5a-container" role="textbox" aria-readonly="true" title="Arizona">Arizona</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div>

                        <form action="#">
                            <div class="form-group">
                                <div class="font-size-xs text-uppercase text-muted mb-3">Reviews</div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        <i class="icon-star-full2"></i>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                        <i class="icon-star-full2"></i>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="font-size-xs text-uppercase text-muted mb-3">Open Status</div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Open
                                        <span class="text-muted ml-1">(83)</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Close
                                        <span class="text-muted ml-1">(83)</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="font-size-xs text-uppercase text-muted mb-3">Active Status</div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Active
                                        <span class="text-muted ml-1">(83)</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Inactive
                                        <span class="text-muted ml-1">(83)</span>
                                    </label>
                                </div>
                            </div>

                            <button type="submit" class="btn bg-blue btn-block">
                                <i class="icon-filter4 mr-2"></i>
                                Filter
                            </button>
                        </form>
                    </div>
                </div>
                <!-- /find course -->

            </div>
            <!-- /sidebar content -->

        </div> --}}
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
@endsection
@push('additional_js_plugin')
    {{-- Checkbox --}}
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/learning.js') }}"></script>
    {{-- Select --}}
    <script src="{{ asset('/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    {{-- Animation --}}
    <script src="{{ asset('/global_assets/js/demo_pages/animations_css3.js') }}"></script>
    {{-- Sweet Alert --}}
    <script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js', []) }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-dashboard').addClass('active');
        });

        function likex(x, pid) {
            var pidx = pid;
            var midx = $('#mid').val();
            var like = $(x);
            var optx = '';

            if (like.hasClass('text-default')) {
                optx = 'like';
            } else if (like.hasClass('text-danger')) {
                optx = 'unlike';
            }

            $.ajax({
                    type: "POST",
                    url: "member/like",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        pid: pidx,
                        mid: midx,
                        opt: optx
                    },
                    dataType: "json"
                })
                .done(function(data) {
                    if (like.hasClass('text-default')) {
                        like.removeClass('text-default');
                        like.addClass('text-danger');
                        optx = 'like';
                        swal({
                            title: "Cool!",
                            text: "Item " + data['product'] + " has been added to wishlist!",
                            icon: "success",
                            button: 'Close'
                        });
                    } else if (like.hasClass('text-danger')) {
                        like.removeClass('text-danger');
                        like.addClass('text-default');
                        optx = 'unlike';
                        swal({
                            title: "Done!",
                            text: "Item " + data['product'] + " has been removed from wishlist!",
                            icon: "info",
                            button: 'Close'
                        });
                    }
                    console.log(data);
                })
                .fail(function() {
                    swal({
                        title: "Opss!",
                        text: "An error acquired!",
                        icon: "error",
                        button: 'Close'
                    });
                });
        }

    </script>
@endpush
