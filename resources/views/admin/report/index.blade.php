@extends('template.template')
@push('additional_css_plugin')
    <link href="{{ url('https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css', []) }}" rel="stylesheet"
        type="text/css">
    <link href="{{ url('https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css', []) }}" rel="stylesheet"
        type="text/css">
@endpush
@push('breadcrumb')
    <h4>
        <i class="icon-arrow-left52 mr-2"></i>
        <span class="font-weight-semibold">Dashboard</span>
    </h4>
@endpush
@section('content')
    <div class="content">
        {{-- Calcendar --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h6 class="card-title">Daterange</h6>
                    </div>

                    <div class="card-body">
                        <div class="input-group">
                            <form action="{{ route('admin.report', []) }}" method="GET">
                                <input type="text" name="date" value="1" class="form-control daterange-buttons mb-1">
                                <span class="input-group-append">
                                    <input class="btn btn-info" type="submit" value="Submit">
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Datatable --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h6 class="card-title">
                            @if (request()->date)
                                Report for : {{ $date_start }} - {{ $date_end }}
                            @else
                                This month report
                            @endif
                        </h6>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="myTable" class="table">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Users</th>
                                        <th>Amount</th>
                                        <th>Payment Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $grand_total = 0;
                                    @endphp
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $item->code }}</td>
                                            <td>
                                                Member : {{ $item->member->name }} <br>
                                                Merchant : {{ $item->merchant->name }}
                                            </td>
                                            <td>{{ $item->totalAmount() }}</td>
                                            <td>{{ $item->paymentType() }}</td>
                                        </tr>
                                        @php
                                            $grand_total += $item->total_amount;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th style="text-align: right">Grand Total</th>
                                        <th style="text-align: left">{{ number_format($grand_total, 2, ',', '.') }}</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('additional_js_plugin')
    {{-- Datepicker --}}
    <script src="{{ asset('/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/picker_date.js') }}"></script>
    {{-- Datatable --}}
    <script src="{{ url('https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js', []) }}"></script>
    <script src="{{ url('https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js', []) }}"></script>
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js', []) }}"></script>
    <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js', []) }}"></script>
    <script src="{{ url('https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js ', []) }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-report').addClass('active');

            $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>
@endpush
