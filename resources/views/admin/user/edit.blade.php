@extends('template.template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('admin.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Edit User
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('admin.user', []) }}" class="btn btn-link btn-float text-default">
        <i class="icon-blocked text-danger"></i> 
        <span>Cancel</span>
    </a>
@endpush
@section('content')
<div class="content">
    @if ($errors->any())
        <!-- Alert -->
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Oh snap!</span> Change a few things up and <a href="#" class="alert-link">try submitting again</a>.
            <br>
            <ul class="mt-3">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <!-- /Alert -->
    @endif
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Edit User Form</h5>
        </div>

        <div class="card-body">
            <form action="{{ route('admin.user.update', [$data->id]) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <fieldset class="mb-6">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Name*</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $data->name) }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Email*</label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email', $data->email) }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Password</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                            <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" placeholder="Fill if only want to update password">
                                <span class="input-group-append">
                                    <span class="input-group-text" onclick="return showPassword(true);"><i class="icon-eye"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Confirm Password</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}" onblur="return validate();" placeholder="Fill if only want to update password">
                                <span class="input-group-append">
                                    <span class="input-group-text" onclick="return showPassword(false);"><i class="icon-eye"></i></span>
                                </span>
                            </div>
                            <span class="form-text text-success" id="password_alert" style="display: none">Success state helper</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Set Role</label>
                        <div class="col-lg-4">
                            <select class="form-control" name="role" id="role" required>
                                <option value="0" {{ $data->role == 0 ? 'selected' : '' }}>Admin</option>
                                <option value="1" {{ $data->role == 1 ? 'selected' : '' }}>Merchant</option>
                                <option value="2" {{ $data->role == 2 ? 'selected' : '' }}>Cashier</option>
                                {{-- <option value="3" {{ $data->role == 3 ? 'selected' : '' }}>Member</option> --}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Active</label>
                        <div class="col-lg-4">
                            <select class="form-control" name="instant_active" id="is_active" required>
                                <option value="1" {{ $data->is_active == 1 ? 'selected' : '' }}>Yes</option>
                                <option value="0" {{ $data->is_active == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label text-lg-right"></label>
                    <div class="col-lg-4">
                        <button type="submit" id="submit" class="btn btn-primary pull-right">
                            Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->

</div>
@endsection
@push('additional_js_plugin')
<script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            console.log('document ready');

            $('#menu-user').addClass('active');
        });

        function validate(){
            var pwd = $('#password');
            var pwd_conf = $('#password_confirmation');
            var alert_text = $('#password_alert');
            var btn_submit = $('#submit');

            if(pwd.val() == pwd_conf.val()){
                pwd_conf.removeClass('border-danger');
                pwd_conf.addClass('border-success');
                alert_text.removeClass('text-danger');
                alert_text.addClass('text-success');
                alert_text.css('display', 'block');
                alert_text.text('Password match');
                btn_submit.removeAttr('disabled');
            }else if(pwd.val() != pwd_conf.val()){
                pwd_conf.removeClass('border-success');
                pwd_conf.addClass('border-danger');
                alert_text.removeClass('text-success');
                alert_text.addClass('text-danger');
                alert_text.css('display', 'block');
                alert_text.text('Password does not match');
                btn_submit.attr('disabled', 'true');
            }
        }

        function showPassword($password)
        {
            console.log('showPassword() function Executed');
            
            var is_password = $password;
            var pwd = $('#password');
            var pwd_conf = $('#password_confirmation');
            
            if(is_password){
                if(pwd.attr('type') === 'password')
                    pwd.attr('type', 'text')
                else
                pwd.attr('type', 'password')
            }else{
                if(pwd_conf.attr('type') === 'password')
                    pwd_conf.attr('type', 'text')
                else
                pwd_conf.attr('type', 'password')
            }
        }
    </script>
@endpush