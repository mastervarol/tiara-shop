@extends('template.template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('admin.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Setting
    </h4>
@endpush
@push('top_right_button')
    
@endpush
@section('content')
<div class="content">
    @if ($errors->any())
        <!-- Alert -->
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Oh snap!</span> Change a few things up and <a href="#" class="alert-link">try submitting again</a>.
            <br>
            <ul class="mt-3">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <!-- /Alert -->
    @endif
    <!-- Alert -->
    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
    </div>
    @endif
    <!-- /Alert -->
    <!-- Form inputs -->
    <div class="card">
        {{-- <div class="card-header header-elements-inline">
            <h5 class="card-title">New Merchant Form</h5>
        </div> --}}

        <div class="card-body">
            <form action="{{ route('admin.setting.update') }}" method="POST" enctype="multipart/form-data">
                @method('POST')
                @csrf
                <fieldset class="mb-6">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card-body text-center">
                                <h6 class="font-weight-semibold mb-0">Store Logo</h6>
					    	</div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-lg-right"></label>
                                <div class="col-lg-8">
                                    <input type="file" name="store_logo" id="store_logo" class="file-input" value="{{ old('store_logo') }}" data-fouc>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Store Name*</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="store_name" id="store_name" value="{{ old('store_name', $data->store_name) }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Store Address*</label>
                                    <div class="col-lg-8">
                                        <textarea name="store_address" id="store_address" cols="30" rows="10" class="form-control" required>{{ old('store_address', $data->store_address) }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Store Phone*</label>
                                    <div class="col-lg-8">
                                        <input type="tel" class="form-control" name="store_phone" id="store_phone" value="{{ old('store_phone', $data->store_phone) }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Message Banner</label>
                                    <div class="col-lg-8">
                                        <textarea name="store_message_banner" id="store_message_banner" cols="30" rows="10" class="form-control">{{ old('store_message_banner', $data->store_message_banner) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-lg-right">Store Open Hours</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control tokenfield" value="
                                        @foreach ($data->store_open_hours as $item)
                                            {{ $item }}
                                        @endforeach
                                        " data-fouc  name="store_open_hours[]" id="store_open_hours">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-body text-center">
					    		<h6 class="font-weight-semibold mb-0">Current Image</h6>
					    	</div>
                            <div class="card">
                                <div class="card-img-actions">
                                    @if ($data->store_logo == '' || $data->store_photo == 'store_logo.png')
                                        <img class="card-img-top img-fluid" src="{{ asset('/global_assets/images/placeholders/placeholder.jpg') }}" alt="">
                                    @else 
                                        <img class="card-img-top img-fluid" src="{{ asset('/global_assets/images/store/'. $data->store_logo) }}" alt="">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <label class="col-lg-1 col-form-label text-lg-right mr-3"></label>
                    <div class="col-lg-4">
                        <button type="submit" id="submit" class="btn btn-primary pull-right">
                            Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                        {{-- <button type="submit" id="submit" class="btn btn-primary pull-right">Submit<i class="icon-paperplane ml-2"></i></button> --}}
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->
</div>
@endsection
@push('additional_js_plugin')
    {{-- File input --}}
    <script src="{{ asset('/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/uploader_bootstrap.js') }}"></script>
    {{-- Tag input --}}
    <script src="{{ asset('/global_assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/forms/tags/tokenfield.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/plugins/ui/prism.min.js') }}"></script>
	<script src="{{ asset('/global_assets/js/demo_pages/form_tags_input.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-setting').addClass('active');
        });

        function confirmDeletion(){
            return confirm('Are you sure want to delete this data?');
        }
    </script>
@endpush