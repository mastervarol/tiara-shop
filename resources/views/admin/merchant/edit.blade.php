@extends('template.template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('admin.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Edit Merchant
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('admin.merchant', []) }}" class="btn btn-link btn-float text-default">
        <i class="icon-blocked text-danger"></i> 
        <span>Cancel</span>
    </a>
@endpush
@section('content')
<div class="content">
    @if ($errors->any())
        <!-- Alert -->
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Oh snap!</span> Change a few things up and <a href="#" class="alert-link">try submitting again</a>.
            <br>
            <ul class="mt-3">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <!-- /Alert -->
    @endif
    <!-- Form inputs -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Edit Merchant Form</h5>
        </div>

        <div class="card-body">
            <form action="{{ route('admin.merchant.update', [$data->id]) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <fieldset class="mb-6">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Photo</label>
                        <div class="col-lg-4">
                            <input type="file" name="photo" id="photo" class="form-control h-auto">
                            @if ($data->photo)
                                <span class="form-text text-success">
                                    Image is available. <a href="{{ asset('/global_assets/images/merchant/'. $data->photo) }}" target="_blank"><i class="icon-eye" title="Check image on new tab"></i></a>
                                </span>
                            @else 
                                <span class="form-text text-danger">Image not available</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Name*</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $data->name) }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Email*</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="email" id="email" value="{{ old('email', $data->email) }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Phone*</label>
                        <div class="col-lg-2">
                            <input type="tel" class="form-control" name="phone" id="phone" value="{{ old('phone', $data->phone) }}" required>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control form-control-uniform-custom" name="has_whatsapp" id="is_whatsapp" required>
                                <option value="" selected hidden disabled>Whatsapp Available?</option>
                                <option value="1" {{ $data->is_whatsapp == 1 ? 'selected' : '' }}>Yes</option>
                                <option value="0" {{ $data->is_whatsapp == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Open*</label>
                        <div class="col-lg-4">
                            <select class="form-control form-control-uniform-custom" name="instant_open" id="is_open" required>
                                {{-- <option value="" selected hidden disabled>Select</option> --}}
                                <option value="1" {{ $data->is_open == 1 ? 'selected' : '' }}>Yes</option>
                                <option value="0" {{ $data->is_open == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label text-lg-right">Active*</label>
                        <div class="col-lg-4">
                            <select class="form-control form-control-uniform-custom" name="instant_active" id="is_active" required>
                                {{-- <option value="" selected hidden disabled>Select</option> --}}
                                <option value="1" {{ $data->is_active == 1 ? 'selected' : '' }}>Yes</option>
                                <option value="0" {{ $data->is_active == 0 ? 'selected' : '' }}>No</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label text-lg-right"></label>
                    <div class="col-lg-4">
                        <button type="submit" id="submit" class="btn btn-primary pull-right">
                            Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->

</div>
@endsection
@push('additional_js_plugin')
<script src="{{ asset('global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            console.log('document ready');

            $('#menu-merchant').addClass('active');
        });
    </script>
@endpush