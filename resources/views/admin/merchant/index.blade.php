@extends('template.template')
@push('additional_css_plugin')
    
@endpush
@push('breadcrumb')
    <h4>
        <a href="{{ route('admin.dashboard', []) }}" style="text-black"><i class="icon-arrow-left52 mr-2"></i> </a>
        <span class="font-weight-semibold">Dashboard</span> - Merchant List
    </h4>
@endpush
@push('top_right_button')
    <a href="{{ route('admin.merchant.create', []) }}" class="btn btn-link btn-float text-default">
        <i class="icon-plus3 text-primary"></i> 
        <span>Add New</span>
    </a>
@endpush
@section('content')
<div class="content">
    <!-- Alert -->
    @if (Session::has('message'))
    <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
    </div>
    @endif
    <!-- /Alert -->
    <div class="card">
        {{-- <div class="card-header header-elements-inline">
            <h5 class="card-title">User List</h5>
            <div class="header-elements">
                <a href="{{ route('admin.user.create', []) }}"><i class="icon-plus3"></i> New</a>
            </div>
        </div> --}}
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th class="wmin-md-10">#</th>
                        <th class="wmin-md-150">Name</th>
                        <th class="wmin-md-30">Phone</th>
                        <th class="wmin-md-30">Status</th>
                        <th class="wmin-md-10">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 0;
                    @endphp
                    @foreach ($data as $item)
                    @php
                        $no++;
                    @endphp
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone ?? '-' }}</td>
                        <td>
                            <strong>Open Status :</strong> {{ $item->isOpen($item->id) }} <br>
                            <strong>Merchant Status :</strong> {{ $item->statusx($item->id) }}
                        </td>
                        <td>
                            <div class="btn-group justify-content-center">
                                <a href="#" class="btn bg-indigo-400 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Options</a>

                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);">
                                    <a href="{{ route('admin.merchant.edit', [$item->id]) }}" class="dropdown-item"><i class="icon-pencil7"></i> Edit</a>
                                    @if ($item->is_open == 1)
                                        <a href="{{ route('admin.merchant.open', ['id' => $item->id, 'opt' => '0']) }}" class="dropdown-item" onclick="return confirm('Close merchant?');"><i class="icon-enter6"></i> Close</a>
                                    @else 
                                        <a href="{{ route('admin.merchant.open', ['id' => $item->id, 'opt' => '1']) }}" class="dropdown-item" onclick="return confirm('Open merchant?');"><i class="icon-esc"></i> Open</a>
                                    @endif
                                    @if ($item->is_active == 1)
                                        <a href="{{ route('admin.merchant.activation', ['id' => $item->id, 'opt' => '0']) }}" class="dropdown-item" onclick="return confirm('Deactive user?');"><i class="icon-user-block"></i> Deactive</a>
                                    @else 
                                        <a href="{{ route('admin.merchant.activation', ['id' => $item->id, 'opt' => '1']) }}" class="dropdown-item" onclick="return confirm('Active user?');"><i class="icon-user-check"></i> Active</a>
                                    @endif
                                    <div class="dropdown-divider"></div>
                                    <form action="{{ route('admin.merchant.delete', [$item->id]) }}" method="POST" enctype="multipart/form-data">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="dropdown-item text-pink" onclick="return confirmDeletion();"><i class="icon-trash"></i>Delete</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <ul class="pagination pagination-flat">
        {{ $data->links() }}
    </ul>
</div>
@endsection
@push('additional_js_plugin')
    
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function(){
            $('#menu-merchant').addClass('active');
        });

        function confirmDeletion(){
            return confirm('Are you sure want to delete this data?');
        }
    </script>
@endpush