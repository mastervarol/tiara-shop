@extends('template.template')
@push('additional_css_plugin')

@endpush
@push('breadcrumb')
    <h4>
        <i class="icon-arrow-left52 mr-2"></i>
        <span class="font-weight-semibold">Dashboard</span>
    </h4>
@endpush
@section('content')
    <div class="content">

    </div>
@endsection
@push('additional_js_plugin')

@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-dashboard').addClass('active');
        });

    </script>
@endpush
