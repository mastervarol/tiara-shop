@extends('template.cashier_template')
@push('additional_css_plugin')

@endpush
@push('breadcrumb')
    <h4>
        <i class="icon-clipboard4 mr-2"></i>
        <span class="font-weight-semibold">{{ $data->code }}</span>
    </h4>
@endpush
@section('content')
    <!-- Content area -->
    <div class="content">
        <!-- Alert -->
        @if (Session::has('message'))
            <div class="alert alert-{{ Session::get('alert-class') }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                <span class="font-weight-semibold">Well done!</span> {{ Session::get('message') }}.
            </div>
        @endif
        <!-- Inner container -->
        <div class="d-flex align-items-start flex-column flex-md-row">

            <!-- Left content -->
            <div class="w-100 order-2 order-md-1">

                <div class="card">
                    <div class="card-header {{ $bg }} text-white header-elements-inline">
                        <h6 class="card-title">
                            <i class="icon-box"></i> Product List
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="wmin-md-10">Product</th>
                                        <th class="wmin-md-10">Qty</th>
                                        <th class="wmin-md-30">Price</th>
                                        <th class="wmin-md-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($data->items as $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->pivot->qty }}</td>
                                            <td>IDR {{ number_format($item->pivot->price, 2, ',', '.') }}</td>
                                            <td>IDR {{ number_format($item->pivot->total_price, 2, ',', '.') }}</td>
                                        </tr>
                                        @php
                                            $total += $item->pivot->total_price;
                                        @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Total</th>
                                        <th>IDR {{ number_format($total, 2, ',', '.') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            <div
                class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

                <!-- Sidebar content -->
                <div class="sidebar-content">

                    <div class="card">
                        <div class="card-header bg-teal-400 header-elements-inline">
                            <span class="card-title font-weight-semibold">MEMBER DETAIL</span>
                        </div>

                        <div class="card-body p-0">
                            <div class="nav nav-sidebar mb-2">
                                {{-- <li class="nav-item-header">
                                <div class="text-uppercase font-size-xs line-height-xs">Merchant</div>
                            </li> --}}
                                <li class="nav-item">
                                    <a href="#" class="nav-link" title="Member Info">
                                        <i class="icon-user"></i>
                                        {{ $data->member->name }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" title="Merchant Info">
                                        <i class="icon-credit-card"></i>
                                        {{ $data->paymentType() }} &nbsp;
                                        @if ($data->status == 1)
                                            <i id="change_payment" class="icon-gear ml-2"
                                                style="color: #26a69a; cursor: help;" title="Change Payment Method"></i>
                                        @endif
                                    </a>
                                </li>
                                <li class="nav-item" id="payment_form" style="display: none">
                                    <select class="form-control w-75 ml-3" name="payment_type_select"
                                        id="payment_type_select">
                                        <option value="0" {{ $data->payment_type == 0 ? 'selected' : '' }}>Credit Card
                                        </option>
                                        <option value="1" {{ $data->payment_type == 1 ? 'selected' : '' }}>Cash</option>
                                        <option value="2" {{ $data->payment_type == 2 ? 'selected' : '' }}>E-Wallet
                                        </option>
                                    </select>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" title="Merchant Info">
                                        <i class="icon-coins"></i>
                                        {{ $data->point_earned }} Pts
                                    </a>
                                </li>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-teal-400 header-elements-inline">
                            <span class="card-title font-weight-semibold">TRANSACTION DETAIL</span>
                        </div>

                        <div class="card-body p-0">
                            <div class="nav nav-sidebar mb-2">
                                <li class="nav-item">
                                    <a href="#" class="nav-link" title="Merchant Info">
                                        <i class="icon-store"></i>
                                        {{ $data->merchant->name }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" title="Cashier Info">
                                        <i class="icon-basket"></i>
                                        {{ count($data->items) }} Item(s)
                                    </a>
                                </li>
                            </div>
                        </div>
                    </div>
                    <!-- Invoice actions -->
                    <div class="card">
                        <div class="card-header bg-teal-400 header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">Options</span>
                            {{-- <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div> --}}
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('cashier.invoice', ['id' => $data->id]) }}"
                                        class="btn bg-teal-400 btn-block btn-float">
                                        <i class="icon-printer icon-2x"></i>
                                        <span>Print invoice</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    {{-- <a class="btn bg-teal-400 btn-block btn-float">
                                    <i class="icon-paperplane icon-2x"></i>
                                    <span>Send Invoice</span>
                                </a> --}}
                                    @if ($data->status == 0)
                                        <form
                                            action="{{ route('cashier.payment.update', [$data->id, 'opt' => request()->opt, 'page' => request()->page]) }}"
                                            method="POST" style="margin-top: 1vh">
                                            @method('POST')
                                            @csrf
                                            <input type="hidden" name="payment_type" id="payment_type">
                                            <button type="submit" onclick="return confirmPayment();"
                                                class="btn bg-success-400 btn-block btn-float">
                                                <i class="icon-coin-dollar icon-2x"></i>
                                                <span>Payment</span>
                                            </button>
                                        </form>
                                    @elseif ($data->status == 1)
                                        <button type="button" onclick="return paid();"
                                            class="btn bg-success-400 btn-block btn-float" style="margin-top: 1vh">
                                            <i class="icon-coin-dollar icon-2x"></i>
                                            <span>Payment</span>
                                        </button>
                                    @else
                                        <button type="button" onclick="return finished();"
                                            class="btn bg-success-400 btn-block btn-float">
                                            <i class="icon-coin-dollar icon-2x"></i>
                                            <span>Payment</span>
                                        </button>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('cashier.dashboard', ['opt' => request()->opt]) }}"
                                        class="btn bg-warning-400 btn-block btn-float" style="margin-top: 1vh">
                                        <i class="icon-blocked icon-2x"></i>
                                        <span>Cancel</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /invoice actions -->

                </div>
                <!-- /sidebar content -->

            </div>
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
    <!-- /content area -->
@endsection
@push('additional_js_plugin')
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/invoice_grid.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            $('#menu-dashboard').addClass('active');

            var change_btn = $('#change_payment');
            var payment_form = $('#payment_form');
            var payment_type_select = $('#payment_type_select');
            var payment_type = $('#payment_type');

            change_btn.on('click', function() {
                if (payment_form.css('display') == 'none')
                    payment_form.css('display', 'block');
                else if (payment_form.css('display') == 'block')
                    payment_form.css('display', 'none');
            });

            payment_type_select.on('change', function() {
                payment_type.val(this.value);
            });
        });

        function confirmPayment() {
            return confirm('Proceed to payment?');
        }

        function paid() {
            return confirm('Order is paid and being proceed by merchant!');
        }

        function finished() {
            return confirm('Transaction finish');
        }

    </script>
@endpush
