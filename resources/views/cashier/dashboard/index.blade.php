@extends('template.cashier_template')
@push('additional_css_plugin')

@endpush
@push('breadcrumb')
    <h4>
        <i class="icon-arrow-left52 mr-2"></i>
        <span class="font-weight-semibold">Dashboard</span>
    </h4>
@endpush
@section('content')
    <!-- Content area -->
    <div class="content">

        <!-- Inner container -->
        <div class="d-flex align-items-start flex-column flex-md-row">

            <!-- Left content -->
            <div class="w-100 order-2 order-md-1">

                <!-- Filter toolbar -->
                <div class="navbar navbar-expand-lg navbar-light navbar-component rounded">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                            data-target="#navbar-filter">
                            <i class="icon-unfold mr-2"></i>
                            Filters
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar-filter">
                        <span class="navbar-text mr-3">
                            Filter:
                        </span>

                        <ul class="navbar-nav flex-wrap">

                            <li class="nav-item dropdown">
                                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-sort-amount-desc mr-2"></i>
                                    By status
                                </a>

                                <div class="dropdown-menu">
                                    <input type="hidden" id="opt" value="{{ request()->opt }}">
                                    <a href="{{ route('cashier.dashboard', ['opt' => 'all']) }}"
                                        class="dropdown-item">Show all orders</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="{{ route('cashier.dashboard', ['opt' => 0]) }}"
                                        class="dropdown-item">Ordered</a>
                                    <a href="{{ route('cashier.dashboard', ['opt' => 1]) }}"
                                        class="dropdown-item">Paid</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /filter toolbar -->
                <!-- Invoice grid -->
                <div class="row" id="data-container">
                    @foreach ($data as $item)
                        <div class="col-lg-3">
                            <div
                                class="card border-left-3 border-left-{{ $item->status == 0 ? 'warning' : 'success' }} rounded-left-0">
                                <div class="card-body">
                                    <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                                        <div>
                                            <h6 class="font-weight-semibold">{{ $item->member->name }}</h6>
                                            <ul class="list list-unstyled mb-0">
                                                <li>Trx ID #: <span class="font-weight-semibold">{{ $item->code }}</span>
                                                </li>
                                                <li>Ordered on: <span
                                                        class="font-weight-semibold">{{ $item->createdDate() }}</span>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">
                                            <h6 class="font-weight-semibold">IDR {{ $item->totalAmount() }}</h6>
                                            <ul class="list list-unstyled mb-0">
                                                <li>Payment : <span
                                                        class="font-weight-semibold">{{ $item->paymentType() }}</span>
                                                </li>
                                                {{-- <li class="dropdown">
                                                    Status: &nbsp;
                                                    <span
                                                        class="badge badge-light badge-striped badge-striped-left border-left-{{ $item->status == 0 ? 'warning' : 'success' }}">{{ $item->cashierTransactionStatus() }}</span>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                    <span>
                                        <span
                                            class="badge badge-mark border-{{ $item->status == 0 ? 'warning' : 'success' }} mr-2"></span>
                                        <span class="font-weight-semibold">
                                            {{ $item->merchant->name }}
                                        </span>
                                    </span>

                                    <ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
                                        <li class="list-inline-item dropdown">
                                            <a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i
                                                    class="icon-menu7"></i></a>

                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a href="{{ route('cashier.payment.form', ['id' => $item->id, 'opt' => request()->opt, 'page' => 'cashier']) }}"
                                                    class="dropdown-item">
                                                    <i class="icon-eye"></i> Detail
                                                </a>
                                                {{-- <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item"><i class="icon-printer"></i> Print invoice</a>
                                        <a href="#" class="dropdown-item"><i class="icon-paperplane"></i> Send invoice</a> --}}
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <!-- Pagination -->
                <div class="d-flex justify-content-center mt-3 mb-3">
                    <ul class="pagination">
                        {{ $data->links() }}
                    </ul>
                </div>
                <!-- /pagination -->

            </div>
            <!-- /left content -->


            <!-- Right sidebar component -->
            {{-- <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- Invoice actions -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Actions</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn bg-teal-400 btn-block btn-float">
                                    <i class="icon-file-plus icon-2x"></i>
                                    <span>New invoice</span>
                                </button>

                                <button type="button" class="btn bg-purple-300 btn-block btn-float">
                                    <i class="icon-archive icon-2x"></i>
                                    <span>Archive</span>
                                </button>
                            </div>
                            
                            <div class="col">
                                <button type="button" class="btn bg-warning-400 btn-block btn-float">
                                    <i class="icon-stats-bars icon-2x"></i>
                                    <span>Statistics</span>
                                </button>

                                <button type="button" class="btn bg-blue btn-block btn-float">
                                    <i class="icon-cog3 icon-2x"></i>
                                    <span>Settings</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /invoice actions -->


                <!-- Navigation -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">Navigation</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body p-0">
                        <div class="nav nav-sidebar mb-2">
                            <li class="nav-item-header">
                                <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-googleplus5"></i>
                                    Create invoice
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-compose"></i>
                                    Edit invoice
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-archive"></i>
                                    Acrhive
                                    <span class="badge badge-pill badge-secondary ml-auto">190</span>
                                </a>
                            </li>

                            <li class="nav-item-header">
                                <div class="text-uppercase font-size-xs line-height-xs">Invoices</div>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-files-empty"></i>
                                    All invoices
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-file-plus"></i>
                                    Pending invoices
                                    <span class="badge badge-pill bg-blue ml-auto">16</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-file-check"></i>
                                    Paid invoices
                                    <span class="badge badge-pill bg-success ml-auto">50</span>
                                </a>
                            </li>
                            <li class="nav-item-divider"></li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-cog3"></i>
                                    Settings
                                </a>
                            </li>
                        </div>
                    </div>
                </div>
                <!-- /navigation -->


                <!-- Filter -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Left checkbox group</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="#">
                            <div class="form-group">
                                <div class="font-weight-semibold mb-3">Amount range:</div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        $0 - $999
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        $1,000 - $1,999
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        $2,000 - $4,999
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        $5,000 - $9,999
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        $10,000 +
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="font-weight-semibold mb-3">Payment method:</div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Wire transfer
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        Paypal
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Payoneer
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        Skrill
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        Cash
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="font-weight-semibold mb-3">Invoice status:</div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Overdue
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        On hold
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" data-fouc>
                                        Pending
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        Paid
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-input-styled" checked data-fouc>
                                        Canceled
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <button type="reset" class="btn btn-light btn-block btn-sm">Reset</button>
                                </div>
                                <div class="col">
                                    <button type="submit" class="btn btn-info btn-block btn-sm">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /filter -->


                <!-- Latest updates -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Latest updates</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <ul class="media-list">
                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon">
                                        <i class="icon-checkmark3"></i>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#">Richard Vango</a> paid invoice #0020
                                    <div class="text-muted font-size-sm">4 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-slate text-slate rounded-round border-2 btn-icon">
                                        <i class="icon-infinite"></i>
                                    </a>
                                </div>
                                
                                <div class="media-body">
                                    Status of invoice <a href="#">#0029</a> has been changed to "On hold"
                                    <div class="text-muted font-size-sm">36 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon">
                                        <i class="icon-checkmark3"></i>
                                    </a>
                                </div>

                                <div class="media-body">
                                    <a href="#">Chris Arney</a> paid invoice #0031 with Paypal
                                    <div class="text-muted font-size-sm">2 hours ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-danger text-danger rounded-round border-2 btn-icon">
                                        <i class="icon-cross2"></i>
                                    </a>
                                </div>
                                
                                <div class="media-body">
                                    Invoice <a href="#">#0041</a> has been canceled
                                    <div class="text-muted font-size-sm">Mar 18, 18:36</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon">
                                        <i class="icon-plus3"></i>
                                    </a>
                                </div>
                                
                                <div class="media-body">
                                    New invoice #0029 has been sent to <a href="#">Beatrix Diaz</a>
                                    <div class="text-muted font-size-sm">Dec 12, 05:46</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /latest updates -->

            </div>
            <!-- /sidebar content -->

        </div> --}}
            <!-- /right sidebar component -->

        </div>
        <!-- /inner container -->

    </div>
    <!-- /content area -->
@endsection
@push('additional_js_plugin')
    <script src="{{ asset('/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/invoice_grid.js') }}"></script>
@endpush
@push('additional_js_script')
    <script>
        $(document).ready(function() {
            console.log('document ready');
            $('#menu-dashboard').addClass('active');

            // getData();
        });

        function getData() {
            console.log('ajax loaded');
            // var opt = $('#opt').val();

            $.get('cashier/ajaxCashierGetData', function(data) {
                // console.log(value.id);
                var container = $('#data-container');

                $.each(data, function(index, value) {
                    var url = `{{ url('cashier/payment-form/`+ value.id +`?page=cashier') }}`;

                    console.log(value.id);
                    container.append('<div class="col-lg-3">' +
                        '<div class="card border-left-3 border-left-success rounded-left-0">' +
                        '<div class="card-body">' +
                        '<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">' +
                        '<div>' +
                        '<h6 class="font-weight-semibold">' + value.member.name + '</h6>' +
                        '<ul class="list list-unstyled mb-0">' +
                        '<li>Trx ID #: <span class="font-weight-semibold">' + value.code +
                        '</span></li>' +
                        '<li>Amount: <span class="font-weight-semibold"> IDR ' + value.total_amount +
                        '</span></li>' +
                        '</ul>' +
                        '</div>' +

                        // '<div class="text-sm-right mb-0 mt-3 mt-sm-0 ml-auto">' +
                        //     '<h6 class="font-weight-semibold">IDR '+ value.total_amount +'</h6>' +
                        //     '<ul class="list list-unstyled mb-0">' +
                        //         '<li>Payment : <span class="font-weight-semibold">'+ value.payment_type +'</span></li>' +
                        //         '<li class="dropdown">' +
                        //             'Status: &nbsp;' +
                        //             '<span class="badge badge-light badge-striped badge-striped-left border-left-success">'+ value.status +'</span>' +
                        //         '</li>' +
                        //     '</ul>' +
                        // '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">' +
                        '<span>' +
                        '<span class="badge badge-mark border-success mr-2"></span>' +
                        '<span class="font-weight-semibold">' +
                        value.merchant.name +
                        '</span>' +
                        '</span>' +

                        '<ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">' +
                        ' <li class="list-inline-item dropdown">' +
                        '<a href="' + url +
                        '" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>' +

                        '<div class="dropdown-menu dropdown-menu-right">' +
                        '<a href="route" class="dropdown-item">' +
                        '<i class="icon-eye"></i> Detail' +
                        '</a>' +
                        // '<div class="dropdown-divider"></div>' +
                        // '<a href="#" class="dropdown-item"><i class="icon-printer"></i> Print invoice</a>' +
                        // '<a href="#" class="dropdown-item"><i class="icon-paperplane"></i> Send invoice</a>' +
                        '</div>' +
                        ' </li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                });
            }, 'json');
        }

    </script>
@endpush
