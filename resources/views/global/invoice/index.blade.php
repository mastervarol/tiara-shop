<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
<link rel="icon" href="{{ asset('global_assets/images/tiara-logo.png') }}">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
<script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="{{ asset('assets/js/app.js') }}"></script>
    
<!-- Content area -->
<body onload="return printx()">
    <div class="content">
        <!-- Invoice template -->
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-4">
                            <img src="{{ asset('/global_assets/images/tiara-logo.png') }}" class="mb-3 mt-2" alt="" style="width: 120px;">
                             <ul class="list list-unstyled mb-0">
                                <li>{{ \App\Models\Setting::find(1)->store_name }}</li>
                                <li>{{ \App\Models\Setting::find(1)->store_address }}</li>
                                <li>{{ \App\Models\Setting::find(1)->store_phone }}</li>
                            </ul>
                        </div>
                    </div>
    
                    <div class="col-sm-6">
                        <div class="mb-4">
                            <div class="text-sm-right">
                                <h4 class="text-primary mb-2 mt-md-2">Invoice #{{ $data->code }}</h4>
                                <ul class="list list-unstyled mb-0">
                                    <li>Date: <span class="font-weight-semibold">{{ $data->createdDate() }}</span></li>
                                    <li>Due date: <span class="font-weight-semibold">{{ $data->createdDate() }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="d-md-flex flex-md-wrap">
                    <div class="mb-4 mb-md-2">
                        <span class="text-muted">Invoice To:</span>
                         <ul class="list list-unstyled mb-0">
                            <li><h5 class="my-2">Name: {{ $data->member->name }}</h5></li>
                            <li>Email: {{ $data->member->email }}</li>
                            <li>Phone: {{ $data->member->phone ?? '-'}}</li>
                        </ul>
                    </div>
    
                    <div class="mb-2 ml-auto">
                        <span class="text-muted">Payment Details:</span>
                        <div class="d-flex flex-wrap wmin-md-400">
                            <ul class="list list-unstyled mb-0">
                                <li><h5 class="my-2">Total Due:</h5></li>
                                <li>Payment method:</li>
                                <li>Payment status:</li>
                            </ul>
    
                            <ul class="list list-unstyled text-right mb-0 ml-auto">
                                <li><h5 class="font-weight-semibold my-2">IDR {{ $data->totalAmount() }}</h5></li>
                                <li>{{ $data->paymentType() }}</li>
                                <li>{{ $data->paymentStatus() }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <th>Item(s)</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $subtotal = 0;
                            $tax = 0;
                            $total = 0;
                        @endphp
                       @foreach ($data->items as $item)
                       <tr align="center">
                            <td>
                                <h6 class="mb-0">{{ $item->name }}</h6>
                            </td>
                            <td>{{ $item->pivot->qty }}</td>
                            <td>IDR {{ $item->pivot->price }}</td>
                            <td><span class="font-weight-semibold">IDR {{ $item->pivot->total_price }}</span></td>
                        </tr>
                        @php
                            $subtotal += $item->pivot->total_price;
                        @endphp
                       @endforeach
                       @php
                            $tax = $subtotal * 0.1;
                            $total = $subtotal + $tax;
                       @endphp
                    </tbody>
                </table>
            </div>
    
            <div class="card-body">
                <div class="d-md-flex flex-md-wrap">
                    <div class="pt-2 mb-3">
                        <h6 class="mb-3">Authorized by</h6>
                        <div class="mb-3">
                            <img src="{{ asset('/global_assets/images/signature.png') }}" width="150" alt="">
                        </div>
    
                        <ul class="list-unstyled text-muted">
                            <li><h6>General Manager</h6></li>
                        </ul>
                    </div>
    
                    <div class="pt-2 mb-3 wmin-md-400 ml-auto">
                        <h6 class="mb-3">Total due</h6>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Subtotal:</th>
                                        <td class="text-right">IDR {{ number_format($subtotal, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tax: <span class="font-weight-normal">(10%)</span></th>
                                        <td class="text-right">IDR {{ number_format($tax, 2, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td class="text-right text-primary"><h5 class="font-weight-semibold">IDR {{ number_format($total, 2, ',', '.') }}</h5></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
    
                        {{-- <div class="text-right mt-3">
                            <button type="button" class="btn btn-primary btn-labeled btn-labeled-left"><b><i class="icon-paperplane"></i></b> Send invoice</button>
                        </div> --}}
                    </div>
                </div>
            </div>
    
            <div class="card-footer">
                <span class="text-muted">Thank you for your purchase.</span>
            </div>
        </div>
        <!-- /invoice template -->
    </div>
    <script>
        function printx()
        {
            var c = confirm("Print invoice?");

            if(c){
                return window.print();
            }else{
                return window.history.back();
            }
        }
    </script>
</body>
<!-- /content area -->