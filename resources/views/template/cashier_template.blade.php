<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tiara Shop - Cashier Panel</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="icon" href="{{ asset('global_assets/images/tiara-logo.png') }}">
    <!-- /global stylesheets -->

    <!-- additional CSS files -->
    @stack('additional_css_plugin')
    <!-- /additional CSS files -->

    <!-- Core JS files -->
    <script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/extra_pnotify.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <!-- /theme JS files -->

    <!-- additional JS files -->
    @stack('additional_js_plugin')
    <!-- /additional JS files -->

    <!-- additional CSS script -->
    @stack('additional_js_script')
    <!-- additional CSS script -->

</head>

<body>
    {{-- Firebase --}}
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js"></script>
    {{-- <script src="{{ asset('global_assets/js/firebase/init.js') }}"></script> --}}
    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyBEqK_0ZG6UdpJiJuYcOn5TVPVjtLBUhZI",
            authDomain: "tiara-shop-f95e3.firebaseapp.com",
            databaseURL: "https://tiara-shop-f95e3.firebaseio.com",
            projectId: "tiara-shop-f95e3",
            storageBucket: "tiara-shop-f95e3.appspot.com",
            messagingSenderId: "1005873255467",
            appId: "1:1005873255467:web:29f70f24aafaf56262c66e"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        /// Retrieve Firebase Messaging object.
        const messaging = firebase.messaging();

        messaging.requestPermission()
            .then(function() {
                console.log('Notification permission granted.');
                // return messaging.getToken();
                // TODO(developer): Retrieve an Instance ID token for use with FCM.
                // ...
            })
            .then(function(token) {
                sessionStorage.setItem('fcm_token', token);
                // console.log(token);
            })
            .catch(function(err) {
                console.log('Unable to get permission to notify.', err);
            });

        // Handle incoming messages. Called when:
        // - a message is received while the app has focus
        // - the user clicks on an app notification created by a service worker
        //   `messaging.setBackgroundMessageHandler` handler.
        messaging.onMessage((payload) => {
            // console.log('Message received. ', payload);

            new PNotify({
                title: payload.data.body,
                text: payload.data.title + '. <a href="' + payload.data.click_action +
                    '">Check new orders</a>',
                icon: 'icon-cart2'
            });
        });

        messaging.getToken({
            vapidKey: 'BPqBLUBlpTjVV_tP3HWXP18lRMRkbtVnb8kuK3F2tbh4Y5_EmUXQE3G8El3tc1pjv57hY3su9ePeva9yVk9pkEE'
        }).then((currentToken) => {
            if (currentToken) {
                console.log(currentToken);
                // alert(token);
                sendTokenToServer(currentToken);
                // updateUIForPushEnabled(currentToken);
            } else {
                // Show permission request.
                console.log('No registration token available. Request permission to generate one.');
                // Show permission UI.
                // updateUIForPushPermissionRequired();
                setTokenSentToServer(false);
            }
        }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // showToken('Error retrieving registration token. ', err);
            setTokenSentToServer(false);
        });

        function sendTokenToServer(currentToken) {
            if (!isTokenSentToServer()) {
                console.log('Sending token to server...');

                axios.post('{{ url('api/save-token/' . Auth::user()->id) }}', {
                        token: currentToken
                    })
                    .then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });

                setTokenSentToServer(true);
                console.log('Token sent');
            } else {
                console.log('Token already sent to server so won\'t send it again ' +
                    'unless it changes');
            }
        }

        function isTokenSentToServer() {
            return window.localStorage.getItem('sentToServer') === '1';
        }

        function setTokenSentToServer(sent) {
            window.localStorage.setItem('sentToServer', sent ? '1' : '0');
        }

    </script>
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="navbar-brand">
            {{-- <a href="#" class="d-inline-block">
				<img src="{{ asset('/global_assets/images/store/'. \App\Models\Setting::find(1)->store_logo) }}" alt="">
			</a> --}}
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                        data-toggle="dropdown">
                        <img src="{{ asset('/global_assets/images/image.png') }}" class="rounded-circle mr-2"
                            height="34" alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        {{-- <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
						<a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
						<a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a> --}}
                        <a href="{{ route('logout', []) }}" class="dropdown-item"><i class="icon-switch2"></i>
                            Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <img src="{{ asset('/global_assets/images/store/' . \App\Models\Setting::find(1)->store_logo) }}"
                            alt="" class="rounded-circle" style="max-height: 100px;">

                    </div>
                    <div class="header-elements d-none">
                        <h4 class="mt-4 ml-3"><i class="icon-user mr-2"></i> <span
                                class="font-weight-semibold">Cashier</span> : {{ Auth::user()->name }}</h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        {{-- <h4><i class="icon-calendar mr-2"></i> <span class="font-weight-semibold">{{ \Carbon\Carbon::now()->format('M d, Y') }}</span></h4> --}}
                    </div>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            @yield('content')
            <!-- /content area -->


            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a
                            href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                    </span>

                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item">
                            <a href="#" class="navbar-nav-link">Text link</a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="navbar-nav-link">
                                <i class="icon-lifebuoy"></i>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov"
                                class="navbar-nav-link font-weight-semibold">
                                <span class="text-pink-400">
                                    <i class="icon-cart2 mr-2"></i>
                                    Purchase
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</body>

</html>
