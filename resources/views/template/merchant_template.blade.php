<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tiara Shop - Merchant Panel</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="icon" href="{{ asset('global_assets/images/tiara-logo.png') }}">
    <link rel="manifest" href="{{ asset('global_assets/manifest.json') }}">
    <!-- /global stylesheets -->

    <!-- additional CSS files -->
    @stack('additional_css_plugin')
    <!-- /additional CSS files -->

    <!-- Core JS files -->
    <script src="{{ asset('global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('/global_assets/js/demo_pages/extra_pnotify.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <!-- /theme JS files -->

    <!-- additional JS files -->
    @stack('additional_js_plugin')
    <!-- /additional JS files -->

    <!-- additional CSS script -->
    @stack('additional_js_script')
    <!-- additional CSS script -->

</head>

<body>
    {{-- Firebase --}}
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js"></script>
    {{-- <script src="{{ asset('global_assets/js/firebase/init.js') }}"></script> --}}
    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyBEqK_0ZG6UdpJiJuYcOn5TVPVjtLBUhZI",
            authDomain: "tiara-shop-f95e3.firebaseapp.com",
            databaseURL: "https://tiara-shop-f95e3.firebaseio.com",
            projectId: "tiara-shop-f95e3",
            storageBucket: "tiara-shop-f95e3.appspot.com",
            messagingSenderId: "1005873255467",
            appId: "1:1005873255467:web:29f70f24aafaf56262c66e"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        /// Retrieve Firebase Messaging object.
        const messaging = firebase.messaging();

        messaging.requestPermission()
            .then(function() {
                console.log('Notification permission granted.');
                // return messaging.getToken();
                // TODO(developer): Retrieve an Instance ID token for use with FCM.
                // ...
            })
            .then(function(token) {
                sessionStorage.setItem('fcm_token', token);
                // console.log(token);
            })
            .catch(function(err) {
                console.log('Unable to get permission to notify.', err);
            });

        // Handle incoming messages. Called when:
        // - a message is received while the app has focus
        // - the user clicks on an app notification created by a service worker
        //   `messaging.setBackgroundMessageHandler` handler.
        messaging.onMessage((payload) => {
            // console.log('Message received. ', payload);

            new PNotify({
                title: payload.data.body,
                text: payload.data.title + '. <a href="' + payload.data.click_action +
                    '">Check new orders</a>',
                icon: 'icon-cart2'
            });
        });

        messaging.getToken({
            vapidKey: 'BPqBLUBlpTjVV_tP3HWXP18lRMRkbtVnb8kuK3F2tbh4Y5_EmUXQE3G8El3tc1pjv57hY3su9ePeva9yVk9pkEE'
        }).then((currentToken) => {
            if (currentToken) {
                console.log(currentToken);
                // alert(token);
                sendTokenToServer(currentToken);
                // updateUIForPushEnabled(currentToken);
            } else {
                // Show permission request.
                console.log('No registration token available. Request permission to generate one.');
                // Show permission UI.
                // updateUIForPushPermissionRequired();
                setTokenSentToServer(false);
            }
        }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // showToken('Error retrieving registration token. ', err);
            setTokenSentToServer(false);
        });

        function sendTokenToServer(currentToken) {
            if (!isTokenSentToServer()) {
                console.log('Sending token to server...');

                axios.post('{{ url('api/save-token/' . Auth::user()->id) }}', {
                        token: currentToken
                    })
                    .then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });

                setTokenSentToServer(true);
                console.log('Token sent');
            } else {
                console.log('Token already sent to server so won\'t send it again ' +
                    'unless it changes');
            }
        }

        function isTokenSentToServer() {
            return window.localStorage.getItem('sentToServer') === '1';
        }

        function setTokenSentToServer(sent) {
            window.localStorage.setItem('sentToServer', sent ? '1' : '0');
        }
    </script>
    <!-- Main navbar -->
    @yield('main_navbar')
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                Navigation
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#"><img
                                        src="{{ asset('global_assets/images/placeholders/placeholder.jpg') }}"
                                        width="38" height="38" class="rounded-circle" alt=""></a>
                            </div>

                            <div class="media-body">
                                <div class="media-title font-weight-semibold">
                                    {{ Auth::user()->name }}
                                </div>
                                <div class="font-size-xs opacity-50">
                                    <i class="icon-pin font-size-sm"></i> &nbsp;Merchant
                                </div>
                            </div>

                            <div class="ml-3 align-self-center">
                                <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <!-- Main -->
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu"
                                title="Main"></i>
                        </li>
                        <li class="nav-item">
                            <a id="menu-dashboard" href="{{ route('merchant.dashboard', []) }}" class="nav-link">
                                <i class="icon-home4"></i>
                                <span>
                                    Dashboard
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="menu-product" href="{{ route('merchant.product', []) }}" class="nav-link">
                                <i class="icon-stack"></i>
                                <span>
                                    Products
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">Transactions & Report</div> <i
                                class="icon-menu" title="Main"></i>
                        </li>
                        <li id="nav-transaction" class="nav-item nav-item-submenu">
                            <a id="menu-transaction" href="{{ route('admin.setting', []) }}" class="nav-link"><i
                                    class="icon-copy"></i> <span>Transactions</span></a>

                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <li class="nav-item">
                                    <a id="menu-transaction-order"
                                        href="{{ route('merchant.transaction', ['opt' => 'order']) }}"
                                        class="nav-link">
                                        <i class="icon-enter"></i>
                                        <span>
                                            Ordered
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <li class="nav-item">
                                    <a id="menu-transaction-finish"
                                        href="{{ route('merchant.transaction', ['opt' => 'finish']) }}"
                                        class="nav-link">
                                        <i class="icon-checkmark4"></i>
                                        <span>
                                            Finished
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                <li class="nav-item">
                                    <a id="menu-transaction-all"
                                        href="{{ route('merchant.transaction', ['opt' => 'all']) }}"
                                        class="nav-link">
                                        <i class="icon-infinite"></i>
                                        <span>
                                            All
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- <li class="nav-item">
							<a id="menu-review" href="{{ route('merchant.review', []) }}" class="nav-link">
								<i class="icon-thumbs-up2"></i>
								<span>
									Reviews
								</span>
							</a>
                        </li> --}}
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">Report</div> <i class="icon-menu"
                                title="Main"></i>
                        </li>
                        <li class="nav-item">
                            <a id="menu-report" href="{{ route('merchant.report', []) }}" class="nav-link">
                                <i class="icon-magazine"></i>
                                <span>
                                    Report
                                </span>
                            </a>
                        </li>
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">More</div> <i class="icon-menu"
                                title="Main"></i>
                        </li>
                        <li class="nav-item">
                            <a id="menu-setting" href="{{ route('merchant.setting', [Auth::user()->merchant->id]) }}"
                                class="nav-link">
                                <i class="icon-cogs"></i>
                                <span>
                                    Settings
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="menu-setting" href="{{ route('logout', []) }}" class="nav-link">
                                <i class="icon-exit"></i>
                                <span>
                                    Logout
                                </span>
                            </a>
                        </li>
                        {{-- <li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-copy"></i> <span>Layouts</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Layouts">
								<li class="nav-item"><a href="index.html" class="nav-link active">Default layout</a></li>
								<li class="nav-item"><a href="layout_2/LTR/default/full/index.html" class="nav-link">Layout 2</a></li>
								<li class="nav-item"><a href="layout_3/LTR/default/full/index.html" class="nav-link">Layout 3</a></li>
								<li class="nav-item"><a href="layout_4/LTR/default/full/index.html" class="nav-link">Layout 4</a></li>
								<li class="nav-item"><a href="layout_5/LTR/default/full/index.html" class="nav-link">Layout 5</a></li>
								<li class="nav-item"><a href="layout_6/LTR/default/full/index.html" class="nav-link disabled">Layout 6 <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
							</ul>
						</li> --}}
                        <!-- /main -->

                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        @stack('breadcrumb')
                    </div>

                    <div class="header-elements d-none">
                        <div class="d-flex justify-content-center">
                            {{-- <a href="#" class="btn btn-link btn-float text-default"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
							<a href="#" class="btn btn-link btn-float text-default"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a> --}}
                            @stack('top_right_button')
                        </div>
                    </div>
                </div>

                {{-- <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<span class="breadcrumb-item active">Dashboard</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					<div class="header-elements d-none">
						<div class="breadcrumb justify-content-center">
							<a href="#" class="breadcrumb-elements-item">
								<i class="icon-comment-discussion mr-2"></i>
								Support
							</a>

							<div class="breadcrumb-elements-item dropdown p-0">
								<a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear mr-2"></i>
									Settings
								</a>

								<div class="dropdown-menu dropdown-menu-right">
									<a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
									<a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
									<a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
									<div class="dropdown-divider"></div>
									<a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
								</div>
							</div>
						</div>
					</div>
				</div> --}}
            </div>
            <!-- /page header -->


            <!-- Content area -->
            @yield('content')
            <!-- /content area -->


            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a
                            href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                    </span>

                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link"
                                target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
                        <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/"
                                class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a>
                        </li>
                        <li class="nav-item"><a
                                href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov"
                                class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i
                                        class="icon-cart2 mr-2"></i> Purchase</span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</body>

</html>
