// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
var firebaseConfig = {
    apiKey: "AIzaSyBEqK_0ZG6UdpJiJuYcOn5TVPVjtLBUhZI",
    authDomain: "tiara-shop-f95e3.firebaseapp.com",
    databaseURL: "https://tiara-shop-f95e3.firebaseio.com",
    projectId: "tiara-shop-f95e3",
    storageBucket: "tiara-shop-f95e3.appspot.com",
    messagingSenderId: "1005873255467",
    appId: "1:1005873255467:web:29f70f24aafaf56262c66e"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = payload.data.title;
    const notificationOptions = {
      body: payload.data.body,
      icon: payload.data.icon,
      click_action: payload.data.click_action
    };
  
    self.registration.showNotification(notificationTitle,
      notificationOptions);
  });
