// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBEqK_0ZG6UdpJiJuYcOn5TVPVjtLBUhZI",
    authDomain: "tiara-shop-f95e3.firebaseapp.com",
    databaseURL: "https://tiara-shop-f95e3.firebaseio.com",
    projectId: "tiara-shop-f95e3",
    storageBucket: "tiara-shop-f95e3.appspot.com",
    messagingSenderId: "1005873255467",
    appId: "1:1005873255467:web:29f70f24aafaf56262c66e"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

/// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

messaging.requestPermission()
.then(function() {
    console.log('Notification permission granted.');
    // return messaging.getToken();
    // TODO(developer): Retrieve an Instance ID token for use with FCM.
    // ...
})
.then(function(token){
    sessionStorage.setItem('fcm_token', token);
    // console.log(token);
})
.catch(function(err) {
    console.log('Unable to get permission to notify.', err);
});

// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage((payload) => {
    // console.log('Message received. ', payload);

    new PNotify({
        title: payload.data.body,
        text: payload.data.title + '. <a href="'+ payload.data.click_action +'">Check new orders</a>',
        icon: 'icon-cart2'
    });
});