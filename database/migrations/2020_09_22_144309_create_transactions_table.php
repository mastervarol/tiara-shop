<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('code', 125)->nullable();
            $table->foreignId('merchant_id')->nullable();
            $table->foreignId('cashier_id')->nullable();
            $table->foreignId('member_id');
            $table->text('note')->nullable();
            $table->integer('status')->comment('0=ordered, 1=confirmed by cashier, 2=all transaction finished')->default(0);
            $table->decimal('total_amount', 20, 2)->nullable()->default(0);
            $table->integer('point_earned')->default(0);
            $table->integer('is_paid')->comment('1=true')->nullable()->default(0);
            $table->integer('payment_type')->comment('0=cc, 1=cash, 2=e-payment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
