<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('photo', 50)->nullable();
            $table->string('name', 75);
            $table->string('email', 50);
            $table->string('code', 50)->nullable();
            $table->string('phone', 15)->nullable();
            $table->integer('is_whatsapp')->default(0)->comment('0=false');
            $table->integer('is_open')->default(0)->comment('0=false');
            $table->integer('is_active')->default(0)->comment('0=false');
            $table->float('review', 5, 1)->comment('scale 1-5')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
