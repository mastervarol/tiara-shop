<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('store_logo', 50)->nullable()->default('store_logo.png');
            $table->string('store_name', 75)->nullable()->default('Store Name');
            $table->string('store_address', 100)->nullable()->default('Store Address');
            $table->string('store_phone', 20)->nullable()->default('0');
            $table->string('store_message_banner', 100)->nullable();
            $table->text('store_open_hours')->comment('array[Monday - 09 s/d 17]')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
