<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('merchant_id');
            $table->string('image', 75)->nullable();
            $table->string('name', 25);
            $table->text('description', 250)->nullable();
            $table->integer('type')->comment('0=food, 1=beverage');
            $table->decimal('sell_price', 20, 2);
            $table->decimal('discount_price', 20, 2)->nullable();
            $table->float('review')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
