<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        DB::table('users')->truncate();
        DB::table('users')->insert(
            [
                'name' => 'Tiara Admin',
                'email' => 'admin@tiaradewata.com',
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'role' => '0',
                'remember_token' => Str::random(10)
            ],
            [
                'name' => 'Merchant 1',
                'email' => 'merchant1@tiaradewata.com',
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'role' => 1,
                'remember_token' => Str::random(10)
            ],
            [
                'name' => 'Cashier 1',
                'email' => 'cashier1@tiaradewata.com',
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'role' => '2',
                'remember_token' => Str::random(10)
            ],
            [
                'name' => 'Member 1',
                'email' => 'member1@tiaradewata.com',
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'role' => '3',
                'remember_token' => Str::random(10)
            ]
        );

        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            'store_logo' => 'store_logo.png',
            'store_name' => 'Store Name',
            'store_address' => 'Store Address',
            'store_phone' => 0
        ]);

    }
}
