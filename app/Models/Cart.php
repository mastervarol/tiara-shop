<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function status()
    {
        if($this->status == 0)
            return 'Created';
        else
            return 'Checked out';
    }
}
