<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function addReview($stars)
    {
        $this->review = $this->review + $stars;
    }

    public function isOpen()
    {
        if($this->is_open)
            return 'Yes';
        else
            return 'No';
    }

    public function statusx()
    {
        if($this->is_active == 1)
            return 'Active';
        else
            return 'Inactive';
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function activeMembers()
    {
        return $this->where('is_active', 1);
    }
}
