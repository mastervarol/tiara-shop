<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use App\Models\Review;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    public function type()
    {
        if ($this->type == 0)
            return 'Food';
        else
            return 'Beverage';
    }

    public function currentPrice()
    {
        if ($this->discount_price)
            return number_format($this->discount_price, 2, ',', '.');
        else
            return number_format($this->sell_price, 2, ',', '.');
    }

    public function sellPrice()
    {
        return number_format($this->sell_price, 2, ',', '.');
    }

    public function discountPrice()
    {
        return number_format($this->discount_price, 2, ',', '.');
    }

    public function reviews()
    {
        // $this->hasMany('App\Models\Review');
        $data = Review::where('product_id', $this->id)
            ->get('review');
        $stars = 0;

        foreach ($data as $d) {
            $stars += $d->review;
        }

        return $stars;
    }

    public function shortDesc()
    {
        return Str::words($this->description, 17, '');
    }

    public function longDesc()
    {
        return $this->description;
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function itemSold()
    {
        return $this->belongsToMany('App\Models\Transaction', 'transaction_items');
    }
}
