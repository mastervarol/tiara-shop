<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaction extends Model
{
    use HasFactory;

    public function member()
    {
        return $this->belongsTo('App\Models\Member')->withTrashed();
    }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    public function cashier()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function transactionStatus()
    {
        if ($this->status == 0)
            return 'Ordered';
        elseif ($this->status == 1)
            return 'Paid';
        else
            return 'Finished';
    }

    public function totalAmount()
    {
        return number_format($this->total_amount, 2, ',', '.');
    }

    public function items()
    {
        return $this->belongsToMany('App\Models\Product', 'transaction_items')->withPivot(
            'qty',
            'price',
            'total_price'
        );
    }

    public function paymentStatus()
    {
        if ($this->is_paid == 1)
            return 'Paid';
        else
            return 'Unpaid';
    }

    public function paymentType()
    {
        if ($this->payment_type == 0)
            return 'Credit Card';
        elseif ($this->payment_type == 1)
            return 'Cash';
        else
            return 'E-Wallet';
    }

    public function cashierTransactionStatus()
    {
        if ($this->status == 0)
            return 'Ordered to merchant';
        elseif ($this->status == 1)
            return 'Finished by merchant';
    }

    public function createdDate()
    {
        return Carbon::parse($this->created_at)->format('M d, Y');
    }
}
