<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'id', 'user_id');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant', 'id', 'user_id');
    }

    public function userRoleName($id)
    {
        $role = $this->find($id)->role;

        if ($role == 0)
            return 'Admin';
        elseif ($role == 1)
            return 'Merchant';
        elseif ($role == 2)
            return 'Cashier';
        else
            return 'Member';
    }

    public function userStatus($id)
    {
        $status = $this->find($id)->is_active;

        if ($status == 1)
            return 'Active';
        else
            return 'Inactive';
    }

    public function memberData()
    {
        if ($this->role == 3) {
            return $this->belongsTo('App\Models\Member');
        }
    }

    public function memberCart()
    {
        if ($this->role == 3) {
            return $this->belongsToThrough('App\Models\Cart', 'App\Models\Member');
        }
    }

    public function memberTransactions()
    {
        if ($this->role == 3) {
            return $this->hasManyThrough('App\Models\Transaction', 'App\Models\Member');
        }
    }

    public function memberReviews()
    {
        if ($this->role == 3) {
            return $this->hasManyThrough('App\Models\Review', 'App\Models\Member');
        }
    }

    public function memberWishlists()
    {
        if ($this->role == 3) {
            return $this->hasManyThrough('App\Models\Wishlist', 'App\Models\Member');
        }
    }

    public function merchantData()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    public function merchantProducts()
    {
        return $this->hasManyThrough('App\Models\Product', 'App\Models\Merhcant');
    }

    public function merchantReviews()
    {
        return $this->hasManyThrough('App\Models\Review', 'App\Models\Merhcant');
    }

    public function merchantTransactions()
    {
        return $this->hasManyThrough('App\Models\Transaction', 'App\Models\Merhcant');
    }

    public function cashierTransactions()
    {
        return $this->hasMany('App\Models\Transaction', 'cashier_id');
    }

    public function setting($id)
    {
        if ($this->role == 0) {
            return $this->belongsTo('App\Models\Setting');
        }
    }
}
