<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Review extends Model
{
    use HasFactory;

    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function createdDate()
    {
        return Carbon::parse($this->created_at)->format('M d, Y');
    }
}
