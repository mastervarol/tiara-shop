<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use HasFactory, SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function wishlists()
    {
        return $this->belongsToMany('App\Models\Product', 'wishlists');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Models\Product', 'carts')->withPivot('qty', 'note', 'status');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function productReviews()
    {
        return $this->belongsToMany('App\Models\Product', 'reviews')->withPivot('review', 'comment');
    }

    public function merchantReviews()
    {
        return $this->belongsToMany('App\Models\Merchant', 'reviews')->withPivot('review', 'comment');
    }

    public function status($id)
    {
        $status = $this->find($id)->is_active;

        if ($status == 1)
            return 'Active';
        else
            return 'Inactive';
    }
}
