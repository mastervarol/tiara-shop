<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Merchant;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MerchantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pagi = $request->pagi ? $request->pagi : 5;
        $data = Merchant::latest()->paginate($pagi);

        return view('admin.merchant.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.merchant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'photo' => 'image|required',
            'name' => 'required',
            'phone' => 'required|numeric',
            'instant_open' => 'required',
            'instant_active' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.merchant.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Merchant();
        $message = 'New merchant has been created successfully';
        $alert_class = 'success';

        $this->newUser($data, $request);

        return redirect()->route('admin.merchant')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return 'yoo';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Merchant::findOrfail($id);

        return view('admin.merchant.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'name' => 'required',
            'phone' => 'required|numeric',
            'instant_open' => 'required',
            'instant_active' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.merchant.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $data = Merchant::findOrfail($id);
        $message = 'Merchant has been created updated successfully';
        $alert_class = 'success';

        $this->saveData($data, $request);

        return redirect()->route('admin.merchant')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Merchant::findOrFail($id);
        $data->user()->delete();
        $data->delete();

        $message = 'Merchant has been deleted';
        $alert_class = 'danger';

        return redirect()->route('admin.merchant')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    private function newUser($data, $request)
    {
        $user = new User();

        $user->name = ucwords($request->name);
        $user->email = $request->email;
        $user->email_verified_at = Carbon::now();
        $user->password = bcrypt('password');
        $user->role = 1;
        $user->is_active = $request->instant_active;

        $user->save();

        $data->user()->associate($user->id);
        $this->saveData($data, $request);
    }

    private function saveData($data, $request)
    {
        $data->name = ucwords($request->name);
        $data->email = $request->email;
        $data->code = 'M-' . Str::upper(Str::random(4));
        $data->phone = $request->phone;
        $data->is_whatsapp = $request->has_whatsapp;
        $data->is_open = $request->instant_open;
        $data->is_active = $request->instant_open;

        $data->save();

        $this->saveImage($data, $request);
    }

    public function activation($id, $opt)
    {
        $data = Merchant::findOrFail($id);
        $message = 'Merchant has been updated successfully';
        $alert_class = 'info';

        $data->is_active = $opt;

        $data->save();

        return redirect()->route('admin.merchant')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    public function open($id, $opt)
    {
        $data = Merchant::findOrFail($id);
        $message = 'Merchant has been updated successfully';
        $alert_class = 'info';

        $data->is_open = $opt;

        $data->save();

        return redirect()->route('admin.merchant')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    private function saveImage($data, $request)
    {
        if ($request->hasFile('photo')) {
            $path = public_path('/global_assets/images/merchant/');
            $image = md5(Str::random(64)) . '.' . $request->file('photo')->getClientOriginalExtension();

            $data->photo = $image;

            Image::make($request->photo)->save($path . $image);

            $data->save();
        }
    }

    public function settingIndex(Request $request, $id)
    {
        $data = Merchant::find($id);

        return view('merchant.setting.index')->with([
            'data' => $data
        ]);
    }

    public function settingUpdate(Request $request, $id)
    {
        $rules = [
            'merchant_name' => 'required',
            'merchant_email' => 'required',
            'merchant_phone' => 'required',
            'has_whatsapp' => 'required',
            'is_open' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->route('merchant.setting')
                ->withErrors($validator)
                ->withInput();
        }

        $data = Merchant::findOrfail($id);
        $message = 'Setting has been updated successfully';
        $alert_class = 'info';

        if ($request->hasFile('merchant_logo')) {
            $path = public_path('/global_assets/images/merchant/');
            $image = md5(Str::random(64)) . '.' . $request->file('merchant_logo')->getClientOriginalExtension();

            $data->photo = $image;

            Image::make($request->merchant_logo)->save($path . $image);
        }

        $data->name = ucwords($request->merchant_name);
        $data->email = $request->merchant_email;
        $data->phone = $request->merchant_phone;
        $data->is_whatsapp = $request->has_whatsapp;
        $data->is_open = $request->is_open;

        $data->save();

        return redirect()->back()->with([
            'data' => $data,
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }
}
