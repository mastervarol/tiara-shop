<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Merchant;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pagi = $request->pagi ? $request->pagi : 5;
        $data = User::latest()->paginate($pagi);
        
        return view('admin.user.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required_with:confirm_password|confirmed',
            'role' => 'required',
            'instant_active' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()
                ->route('admin.user.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new User();
        $message = 'New user has been created successfully';
        $alert_class = 'success';

        $this->saveData($data, $request);

        return redirect()->route('admin.user')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = User::findOrFail($id);

        return view('admin.user.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = User::findOrFail($id);
        $message = 'User has been updated successfully';
        $alert_class = 'info';

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'instant_active' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()
                ->route('admin.user.edit', [$id])
                ->withErrors($validator)
                ->withInput();
        }

        $this->saveData($data, $request);

        return redirect()->route('admin.user')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::findOrFail($id)->delete();
        $message = 'User has been deleted';
        $alert_class = 'danger';

        return redirect()->route('admin.user')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    private function saveData($data, $request)
    {
        $data->name = ucwords($request->name);
        $data->email = $request->email;
        $data->email_verified_at = Carbon::now();
        if($request->password){
            $data->password = bcrypt($request->password);
        }
        $data->role = $request->role;
        $data->is_active = $request->instant_active;

        $data->save();
        // dd($data->id);
        if($request->role == 1){
            $this->newMerchant($request, $data->id);
        }
    }

    private function newMerchant($request, $user)
    {
        $data = new Merchant();

        $data->user()->associate($user);
        $data->name = ucwords($request->merchant_name);
        $data->email = $request->email;
        $data->code = 'M-'.Str::upper(Str::random(4));

        $data->save();
    }

    public function activation($id, $opt)
    {
        $data = User::findOrFail($id);
        $message = 'User has been updated successfully';
        $alert_class = 'info';

        $data->is_active = $opt;

        $data->save();

        return redirect()->route('admin.user')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }
}
