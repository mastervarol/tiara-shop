<?php

namespace App\Http\Controllers;

use App\Mail\OrderComplated;
use App\Models\Member;
use App\Models\Merchant;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $opt = $request->opt;
        $pagi = $request->pagi ? $request->pagi : 5;
        $mid = Auth::user()->merchant->id;

        if ($opt == 'order')
            $data = Transaction::where('status', 1)
                ->whereHas('merchant', function ($q) use ($mid) {
                    $q->where('merchant_id', $mid);
                })->latest()->paginate($pagi);
        elseif ($opt == 'finish')
            $data = Transaction::where('status', 2)
                ->whereHas('merchant', function ($q) use ($mid) {
                    $q->where('merchant_id', $mid);
                })->latest()->paginate($pagi);
        else
            $data = Transaction::whereHas('merchant', function ($q) use ($mid) {
                $q->where('merchant_id', $mid);
            })->with('member')
                ->latest()->paginate($pagi);

        return view('merchant.transaction.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $page = $request->page;
        $opt = $request->opt;

        $data = Transaction::where('id', $id)
            ->with(['member', 'merchant', 'cashier', 'items'])
            ->first();
        $bg = $data->status == 0 ? 'bg-warning-400' : 'bg-teal-400';

        if ($page == 'merchant') {
            return view('merchant.transaction.show')->with([
                'data' => $data,
                'opt' => $opt,
                'bg' => $bg
            ]);
        } elseif ($page == 'cashier') {
            return view('cashier.transaction.index')->with([
                'data' => $data,
                'opt' => $opt,
                'bg' => $bg
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Transaction::findOrfail($id);
        $opt = $request->opt;
        $page = $request->page;
        $alert_class = 'info';

        if ($opt == 'finish' && $page == 'merchant') {
            $message = 'Transaction ' . $data->code . ' has been marked as finish';
            $data->status = 2;

            $this->notifyMember($id);

            if (Mail::failures()) {
                Session::flash('messageType', 'danger');
                Session::flash('message', 'Failed to send email');
                return redirect()->back();
            }
        }
        if ($page == 'cashier') {
            $message = 'Transaction ' . $data->code . ' has been marked as paid';
            $data->status = 1;
            $data->is_paid = 1;
            if ($request->payment_type) {
                $data->payment_type = $request->payment_type;
            }
            // return $data->merchant->user->fcm_token;
            $this->notify($data->merchant->user->fcm_token, $data->member->name);
            $data->cashier()->associate(Auth::id());
        }

        $data->save();

        return redirect()->back()->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function report(Request $request)
    {
        $query = Transaction::query();
        $date = explode(' - ', $request->date);

        $date_start = $date[0] ?? null;
        $date_end = $date[1] ?? null;

        $query->when(!isset($request->date), function ($q) {
            return $q->where('created_at', '>=', Carbon::now()->firstOfMonth()->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::now()->endOfMonth()->format('Y-m-d'));
        });
        $query->when(isset($request->date), function ($q) use ($date_start, $date_end) {
            return $q->where('created_at', '>=', Carbon::parse($date_start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($date_end)->format('Y-m-d'));
        });
        $data = $query
            ->where('status', 2)
            ->get();

        return view('admin.report.index')->with([
            'data' => $data,
            'date_start' => $date_start,
            'date_end' => $date_end
        ]);
    }

    public function ajaxCashierGetData()
    {
        $data = Transaction::where('status', 1)
            ->with([
                'member',
                'merchant'
            ])
            ->orderBy('id')
            ->get();

        return response()->json($data);
    }

    public function getInvoice($id)
    {
        $data = Transaction::find($id);

        return view('global.invoice.index')->with([
            'data' => $data
        ]);
    }

    public function notifyMember($tid)
    {
        $trx = Transaction::findOrfail($tid);
        $data = [
            'trx_code' => $trx->code,
            'member_name' => $trx->member->name,
            'merchant_name' => $trx->merchant->name
        ];

        Mail::to($trx->member->email)
            ->send(new OrderComplated($data));
    }

    private function notify($token, $member_name)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        // prepare the message
        $message = array(
            'icon' => 'http://localhost/tiara-shop/public/global_assets/images/tiara-logo.png',
            'body' => 'Incoming Order',
            'title' =>  $member_name . ' made a new order',
            'click_action' => 'http://localhost/tiara-shop/public/merchant/transaction?opt=order'

        );
        $fields = array(
            'to'               => $token,
            'data'             => $message
        );
        $headers = array(
            'Authorization: key=AAAA6jK31Cs:APA91bHRNro3WOCfzY4YpKS3jAq-xOTwvEylC8LSIcHyAOMfSCwIdxxk_XH0hkdAGrCMqf0a1BHoRY8eZJ9Y-FhnMRhSLlNiQG61KzfqTEDujorKUxKdx_CWFUMddgYmbYm19CPG5lzv',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    public function merchantReport(Request $request)
    {
        $query = Transaction::query();
        $date = explode(' - ', $request->date);

        $date_start = $date[0] ?? null;
        $date_end = $date[1] ?? null;

        $query->when(!isset($request->date), function ($q) {
            return $q->where('created_at', '>=', Carbon::now()->firstOfMonth()->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::now()->endOfMonth()->format('Y-m-d'));
        });
        $query->when(isset($request->date), function ($q) use ($date_start, $date_end) {
            return $q->where('created_at', '>=', Carbon::parse($date_start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($date_end)->format('Y-m-d'));
        });
        $data = $query
            ->where('status', 2)
            ->where('merchant_id', Auth::user()->merchant->id)
            ->get();

        return view('merchant.report.index')->with([
            'data' => $data,
            'date_start' => $date_start,
            'date_end' => $date_end
        ]);
    }
}
