<?php

namespace App\Http\Controllers;

use App\Models\Setting;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    //
    public function index()
    {
        $data = Setting::findOrfail(1);

        return view('admin.setting.index')->with([
            'data' => $data
        ]);
    }

    public function update(Request $request)
    {
        $rules = [
            'store_name' => 'required',
            'store_address' => 'required',
            'store_phone' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.setting')
                ->withErrors($validator)
                ->withInput();
        }

        $data = Setting::findOrfail(1);
        $message = 'Setting has been updated successfully';
        $alert_class = 'info';

        if ($request->hasFile('store_logo')) {
            $path = public_path('/global_assets/images/store/');
            $image = md5(Str::random(64)) . '.' . $request->file('store_logo')->getClientOriginalExtension();

            $data->store_logo = $image;

            Image::make($request->store_logo)->save($path . $image);
        }

        $data->store_name = ucwords($request->store_name);
        $data->store_address = $request->store_address;
        $data->store_phone = $request->store_phone;
        $data->store_message_banner = $request->store_message_banner;
        $data->store_open_hours = $request->store_open_hours;

        $data->save();

        return redirect()->route('admin.setting')->with([
            'data' => $data,
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }
}
