<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pagi = $request->pagi ? $request->pagi : 5;
        $mid  = Auth::user()->merchant->id;
        
        $data = Product::whereHas('merchant', function($q) use ($mid){
                $q->where('merchant_id', $mid);
            })
            ->latest()
            ->paginate($pagi);
        
        return view('merchant.product.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('merchant.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'image' => 'image|required',
            'name' => 'required',
            'description' => 'required',
            'type' => 'required',
            'sell_price' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()
                ->route('merchant.product.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Product();
        $mid = Auth::user()->merchant->id;
        $message = 'New product has been created successfully';
        $alert_class = 'success';
        
        $data->merchant()->associate($mid);
        $this->saveData($data, $request);

        return redirect()->route('merchant.product')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Product::findOrfail($id);

        return view('merchant.product.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'type' => 'required',
            'sell_price' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()
                ->route('merchant.product.edit', [$id])
                ->withErrors($validator)
                ->withInput();
        }

        $data = Product::findOrFail($id);
        $message = 'Product has been updated successfully';
        $alert_class = 'success';

        $this->saveData($data, $request);

        return redirect()->route('merchant.product')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::findOrFail($id)->delete();

        $message = 'Merchant has been deleted';
        $alert_class = 'danger';

        return redirect()->route('admin.merchant')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    private function saveData($data, $request)
    {
        $data->name = ucwords($request->name);
        $data->description = $request->description;
        $data->type = $request->type;
        $data->sell_price = $request->sell_price;
        $data->discount_price = $request->discount_price;

        $data->save();
        
        $this->saveImage($data, $request);
    }

    private function saveImage($data, $request)
    {
        if($request->hasFile('image')){
            $path = public_path('/global_assets/images/product/');
            $image = md5(Str::random(64)). '.' .$request->file('image')->getClientOriginalExtension();

            $data->image = $image;

            Image::make($request->image)->save($path.$image);

            $data->save();
        }
    }
}
