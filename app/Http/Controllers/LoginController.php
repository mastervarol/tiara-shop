<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use App\Models\Member;

class LoginController extends Controller
{
    //
    public function index()
    {
        return view('login');
    }

    public function proceed(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('login')
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 1], true)) {
            // Authentication passed...
            if (Auth::user()->role == 0)
                return redirect('admin');
            elseif (Auth::user()->role == 1)
                return redirect('merchant');
            elseif (Auth::user()->role == 2)
                return redirect('cashier');
            elseif (Auth::user()->role == 3)
                return redirect('member');
        } else {
            // validation not successful, send back to form 
            return redirect('login');
        }
    }

    public function registerForm()
    {
        return view('register');
    }

    public function porceedRegister(Request $request)
    {
        $message = 'Registration success, please login using your credentials';
        $alert_class = 'success';
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required_with:password_confirmation|confirmed'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $user->name = ucwords($request->name);
        $user->email = $request->email;
        $user->email_verified_at = Carbon::now();
        $user->password = bcrypt($request->password);
        $user->role = 3;
        $user->is_active = 1;

        $user->save();

        $member = new Member();
        $member->name = ucwords($request->name);
        $member->email = $request->email;
        $member->is_active = 1;
        $member->user()->associate($user->id);

        $member->save();

        return redirect()->route('login')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    public function logout(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        unset($user->remember_token);

        Auth::logout();

        $request->session()->invalidate();

        return redirect('login');
    }

    public function migrate()
    {
        Artisan::call('migrate');

        dd('migrated');
    }
}
