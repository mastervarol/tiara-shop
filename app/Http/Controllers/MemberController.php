<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\Review;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Wishlist;
use Illuminate\Support\Str;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pagi = $request->pagi ? $request->pagi : 5;
        $data = Member::latest()->paginate($pagi);

        return view('admin.member.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'instant_active' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.member.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Member();
        $message = 'New member has been created successfully';
        $alert_class = 'success';

        $this->newUser($data, $request);

        return redirect()->route('admin.member')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Member::findOrFail($id);

        return view('admin.member.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'instant_active' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.member.edit', [$id])
                ->withErrors($validator)
                ->withInput();
        }

        $data = Member::findOrFail($id);
        $message = 'Member has been udpated successfully';
        $alert_class = 'info';

        return redirect()->route('admin.member')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Member::findOrFail($id);
        $data->user()->delete();
        $data->delete();

        $message = 'Member has been deleted';
        $alert_class = 'danger';

        return redirect()->route('admin.member')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    private function newUser($data, $request)
    {
        $user = new User();

        $user->name = ucwords($request->name);
        $user->email = $request->email;
        $user->email_verified_at = Carbon::now();
        $user->password = bcrypt('password');
        $user->role = 3;
        $user->is_active = $request->instant_active;

        $user->save();

        $data->user()->associate($user->id);
        $this->saveData($data, $request);
    }

    private function saveData($data, Request $request)
    {
        $data->name = ucwords($request->name);
        $data->email = $request->email;
        $data->is_active = $request->instant_active;

        $data->save();
    }

    public function activation($id, $opt)
    {
        $data = Member::findOrFail($id);
        $message = 'User has been updated successfully';
        $alert_class = 'info';

        $data->is_active = $opt;

        $data->save();

        return redirect()->route('admin.member')->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    public function dashboard(Request $request)
    {
        $merchants = Merchant::All();
        $products = Product::whereHas('merchant')->get();
        // return $merchants;
        return view('member.index')->with([
            'merchants' => $merchants,
            'products' => $products
        ]);
    }

    public function like(Request $request)
    {
        $member = Member::find($request->mid);
        $product = Product::findOrfail($request->pid);
        $opt = $request->opt;
        $data = array();
        $data['product'] = $product->name;
        $data['opt'] = $opt;

        if ($opt == 'like') {
            $member->wishlists()->attach($product);
        } else {
            $member->wishlists()->detach($product);
        }

        return response()->json($data);
    }

    public function cart(Request $request)
    {
        $member = Member::find($request->mid);
        $product = Product::findOrfail($request->pid);
        $opt = $request->opt;
        $data = array();
        $data['product'] = $product->name;
        $data['opt'] = $opt;
        // return response($product);
        if ($opt == 'add') {
            $member->carts()->attach($product);
        } else {
            $member->carts()->detach($product);
        }

        return response()->json($data);
    }

    public function merchant(Request $request)
    {
        $name = $request->name;
        $review = $request->review;
        $open = $request->is_open;

        $query = Merchant::query();

        $query->when($name, function ($q) use ($name) {
            return $q->where('name', 'LIKE', '%' . $name . '%');
        });
        $query->when($review, function ($q) use ($review) {
            if ($review == 1)
                return $q->where('review', '>', 3);
            else
                return $q->where('review', '<', 3);
        });
        $query->when($open, function ($q) use ($open) {
            if ($open == 1)
                return $q->where('is_open', 1);
            else
                return $q->where('is_open', '');
        });

        $data = $query->get();

        return view('member.merchant.index')->with([
            'data' => $data
        ]);
    }

    public function merchantDetail($id)
    {
        $data = Merchant::findOrfail($id);

        return view('member.merchant.show')->with([
            'data' => $data
        ]);
    }

    public function product(Request $request)
    {
        $type = $request->type;
        $discounted = $request->discounted;
        $review = $request->review;

        $query = Product::query();

        $query->when($type, function ($q) use ($type) {
            if ($type == 1)
                return $q->where('type',  $type);
            elseif ($type == 2)
                return $q->where('type',  '0');
            else
                return $q;
        });
        $query->when($discounted, function ($q) use ($discounted) {
            return $q->where('discount_price',  '<>', '');
        });
        $query->when($review, function ($q) use ($review) {
            if ($review == 1)
                return $q->where('review', '>', 3);
            else
                return $q->where('review', '<', 3);
        });

        $data = $query->get();

        return view('member.product.index')->with([
            'data' => $data
        ]);
    }

    public function productDetail($id)
    {
        $uid = Auth::user()->member->id;
        $data = Product::findOrfail($id);
        $review =  Review::where('member_id', $uid)
            ->where('product_id', $id)
            ->first();

        return view('member.product.show')->with([
            'data' => $data,
            'review' => $review
        ]);
    }

    public function submitReview(Request $request)
    {
        $uid = User::findOrfail($request->uid)->member->id;
        $pid = $request->pid;
        $stars = $request->stars;

        $data = Review::where('member_id', $uid)
            ->where('product_id', $pid)
            ->first();

        if ($data) {
            $data->member_id = $uid;
            $data->product_id = $pid;
            $data->review = $stars;

            $data->save();

            return response()->json($data);
        } else {
            $new = new Review();
            $new->member_id = $uid;
            $new->product_id = $pid;
            $new->review = $stars;

            $new->save();

            return response()->json($new);
        }
    }

    public function profile($id)
    {
        $user = User::findOrfail($id);
        $data = $user->member;

        return view('member.profile')->with([
            'data' => $data
        ]);
    }

    public function profileUpdate(Request $request, $id)
    {
        // return 'yoo';
        $data = Member::findOrfail($id);
        $user = User::findOrfail($data->user->id);
        $message = 'Profile has been updated';
        $alert_class = 'success';

        $rules = [
            'name' => 'required',
            'email' => 'required|email'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data->name = ucwords($request->name);
        $data->email = $request->email;
        $data->phone = $request->phone;

        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password && $request->password_confirmation) {
            $rulesx = [
                'password' => 'required_with:password_confirmation|confirmed'
            ];

            $validatorx = Validator::make($request->all(), $rulesx);

            if ($validatorx->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validatorx)
                    ->withInput();
            }

            $user->password = bcrypt($request->password);
        }

        $data->save();
        $user->save();

        return redirect()->back()->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    public function checkoutForm($id)
    {
        $data = Member::findOrfail($id);

        return view('member.checkout')->with([
            'data' => $data
        ]);
    }

    public function removeItem($id, $pid)
    {
        //
        $message = 'Item removed';
        $alert_class = 'danger';
        $data = Cart::where('member_id', $id)
            ->where('product_id', $pid)
            ->first();
        // return $id;
        $data->delete();

        return redirect()->back()->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    public function proceedCheckout(Request $request, $id)
    {
        $data = Member::findOrfail($id);
        $cashier = User::where('role', 2)
            ->whereNotNull('remember_token')
            ->first('fcm_token');
        // $items = $data->carts;
        // $token = User::find(2)->fcm_token;
        // return $this->notify($token, $data->name);
        $items = $request->item;
        $trx = new Transaction();
        $points = 0;
        $total_amount = 0;
        $message = 'Transaction success';
        $alert_class = 'success';
        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('m');
        // return $items;
        $trx->member()->associate($id);
        $trx->note = $request->note;
        $trx->payment_type = $request->payment_type;
        $trx->save();

        // foreach($items as $x){
        for ($i = 0; $i < count($items); $i++) {
            $item = Product::findOrfail($items[$i]);

            $trx->code = $year . '/' . $month . '/' . $item->merchant->code . '/' . Str::upper(Str::random(5));

            $trx->merchant()->associate($item->merchant->id);
            $total_amount = $total_amount + ($item->discount_price ?? $item->sell_price * $request->qty[$i]);

            $trx_item = new TransactionItem();
            $trx_item->transaction()->associate($trx->id);
            $trx_item->item()->associate($item);
            $trx_item->qty = $request->qty[$i];
            $trx_item->price = $item->discount_price ?? $item->sell_price;
            $trx_item->total_price = $item->discount_price ?? $item->sell_price * $request->qty[$i];

            $trx_item->save();

            $data->carts()->detach($item);
            // $token = $item->merchant->user->fcm_token;
            // $this->notify($token, $data->name);
        }

        $trx->total_amount = $total_amount;
        $points = $total_amount * 0.001;
        $trx->point_earned = $points;
        $data->points += $points;

        $trx->save();
        $data->save();

        $this->notify($cashier->fcm_token, $data);

        return redirect()->back()->with([
            'message' => $message,
            'alert-class' => $alert_class
        ]);
    }

    private function notify($token, $member)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        // prepare the message
        $message = array(
            'icon' => 'http://localhost/tiara-shop/public/global_assets/images/tiara-logo.png',
            'body' => 'Incoming Order',
            'title' =>  $member->name . ' made a new order',
            'click_action' => 'http://localhost/tiara-shop/public/cashier/' . $member->id . '?page=cahsier'

        );
        $fields = array(
            'to'               => $token,
            'data'             => $message
        );
        $headers = array(
            'Authorization: key=AAAA6jK31Cs:APA91bHRNro3WOCfzY4YpKS3jAq-xOTwvEylC8LSIcHyAOMfSCwIdxxk_XH0hkdAGrCMqf0a1BHoRY8eZJ9Y-FhnMRhSLlNiQG61KzfqTEDujorKUxKdx_CWFUMddgYmbYm19CPG5lzv',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }
}
