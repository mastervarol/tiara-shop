<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;

class DashboardController extends Controller
{
    //
    public function admin(Request $request)
    {
        return view('admin.dashboard.index');
    }

    public function merchant(Request $request)
    {
        return view('merchant.dashboard.index');
    }

    public function cashier(Request $request)
    {
        $opt = $request->opt;
        $pagi = $request->pagi ? $request->pagi : 8;

        $query = Transaction::query();

        $query->when($request->opt == 'all', function ($q) {
            return $q->where('status', '<>', 3);
        });
        $query->when($request->opt == 1, function ($q) {
            return $q->where('status', 2);
        });
        $query->when(!$request->opt, function ($q) {
            return $q->where('status', 0);
        });

        $data = $query->with('member', 'merchant')
            ->paginate($pagi);

        return view('cashier.dashboard.index')->with([
            'data' => $data
        ]);
    }

    public function member(Request $request)
    {
        return view('member.dashboard.index');
    }
}
