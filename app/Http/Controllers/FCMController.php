<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class FCMController extends Controller
{
    //
    public function checkFcmToken($id)
    {
        $data = User::findOrfail($id);

        if($data){
            return response()->json([
                'fcm_token' => $data->fcm_token
            ]);
        }else{
            return false;
        }
    }

    public function saveFcmToken(Request $request, $id)
    {
        $data = User::findOrfail($id);

        $data->fcm_token = $request->token;
        $data->save();

        return response()->json([
            'message' => 'User token is updated'
        ]);
    }
}
