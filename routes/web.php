<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\ReviewController;
use App\Models\Transaction;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('login', [LoginController::class, 'index'])->name('login');
Route::post('login', [LoginController::class, 'proceed'])->name('login.proceed');
Route::get('register', [LoginController::class, 'registerForm'])->name('register');
Route::post('register', [LoginController::class, 'porceedRegister'])->name('register.proceed');

Route::get('migrate', [LoginController::class, 'migrate']);
Route::middleware(['auth'])->group(function () {
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
    // Admin
    Route::prefix('admin')->group(function () {
        // Dashboard
        Route::get('/', [DashboardController::class, 'admin'])->name('admin.dashboard');
        // User
        Route::resource('user', UserController::class, [
            'names' => [
                'as' => 'prefix',
                'index' => 'admin.user',
                'create' => 'admin.user.create',
                'store' => 'admin.user.store',
                'edit' => 'admin.user.edit',
                'update' => 'admin.user.update',
                'destroy' => 'admin.user.delete'
            ]
        ]);
        Route::get('user/{id}/activation/{opt}', [UserController::class, 'activation'])->name('admin.user.activation');
        // Member
        Route::resource('member', MemberController::class, [
            'names' => [
                'as' => 'prefix',
                'index' => 'admin.member',
                'create' => 'admin.member.create',
                'store' => 'admin.member.store',
                'edit' => 'admin.member.edit',
                'update' => 'admin.member.update',
                'destroy' => 'admin.member.delete'
            ]
        ]);
        Route::get('member/{id}/activation/{opt}', [MemberController::class, 'activation'])->name('admin.member.activation');
        // Merchant
        Route::resource('merchant', MerchantController::class, [
            'names' => [
                'as' => 'prefix',
                'index' => 'admin.merchant',
                'create' => 'admin.merchant.create',
                'store' => 'admin.merchant.store',
                'edit' => 'admin.merchant.edit',
                'update' => 'admin.merchant.update',
                'destroy' => 'admin.merchant.delete'
            ]
        ]);
        Route::get('merchant/{id}/activation/{opt}', [MerchantController::class, 'activation'])->name('admin.merchant.activation');
        Route::get('merchant/{id}/open/{opt}', [MerchantController::class, 'open'])->name('admin.merchant.open');
        //Report
        Route::get('report', [TransactionController::class, 'report'])->name('admin.report');
        //Setting
        Route::get('setting', [SettingController::class, 'index'])->name('admin.setting');
        Route::post('setting/update', [SettingController::class, 'update'])->name('admin.setting.update');
    });
    // Merchant
    Route::prefix('merchant')->group(function () {
        //Dashboard
        Route::get('/', [DashboardController::class, 'merchant'])->name('merchant.dashboard');
        // Product
        Route::resource('product', ProductController::class, [
            'names' => [
                'as' => 'prefix',
                'index' => 'merchant.product',
                'create' => 'merchant.product.create',
                'store' => 'merchant.product.store',
                'edit' => 'merchant.product.edit',
                'update' => 'merchant.product.update',
                'destroy' => 'merchant.product.delete'
            ]
        ]);
        // Transaction
        Route::resource('transaction', TransactionController::class, [
            'names' => [
                'as' => 'prefix',
                'index' => 'merchant.transaction',
                'show' => 'merchant.transaction.show',
                'update' => 'merchant.transaction.update'
            ]
        ]);
        //Report
        Route::get('report', [TransactionController::class, 'merchantReport'])->name('merchant.report');
        // Review
        Route::resource('review', ReviewController::class, [
            'names' => [
                'as' => 'prefix',
                'index' => 'merchant.review',
                'show' => 'merchant.review.show'
            ]
        ]);
        //Setting
        Route::get('setting/{id}', [MerchantController::class, 'settingIndex'])->name('merchant.setting');
        Route::post('setting/update/{id}', [MerchantController::class, 'settingUpdate'])->name('merchant.setting.update');
    });
    // Cashier
    Route::prefix('cashier')->group(function () {
        //Dashboard
        Route::get('/', [DashboardController::class, 'cashier'])->name('cashier.dashboard');
        Route::get('ajaxCashierGetData', [TransactionController::class, 'ajaxCashierGetData'])->name('cashier.ajax.get_data');
        Route::get('payment-form/{id}', [TransactionController::class, 'show'])->name('cashier.payment.form');
        Route::post('payment-update/{id}', [TransactionController::class, 'update'])->name('cashier.payment.update');
        Route::get('invoice/{id}', [TransactionController::class, 'getInvoice'])->name('cashier.invoice');
    });
    // Member
    Route::prefix('member')->group(function () {
        //Dashboard
        Route::get('/', [MemberController::class, 'dashboard'])->name('member.dashboard');
        Route::post('/like', [MemberController::class, 'like'])->name('member.like');
        Route::post('/reviewProduct', [MemberController::class, 'submitReview'])->name('member.review.product');
        Route::post('/cart', [MemberController::class, 'cart'])->name('member.cart');
        Route::get('/merchant', [MemberController::class, 'merchant'])->name('member.merchant');
        Route::get('/merchant/{id}', [MemberController::class, 'merchantDetail'])->name('member.merchant.show');
        Route::get('/product', [MemberController::class, 'product'])->name('member.product');
        Route::get('/product/{id}', [MemberController::class, 'productDetail'])->name('member.product.show');
        Route::get('/profile/{id}', [MemberController::class, 'profile'])->name('member.profile');
        Route::post('/profile/{id}/update', [MemberController::class, 'profileUpdate'])->name('member.profile.update');
        Route::get('/checkout/{id}', [MemberController::class, 'checkoutForm'])->name('member.checkout');
        Route::get('/checkout/{id}/product/{pid}', [MemberController::class, 'removeItem'])->name('member.removeItem');
        Route::post('/checkout/{id}/proceed', [MemberController::class, 'proceedCheckout'])->name('member.checkout.proceed');
        // Route::post('/checkout/{id}/notify', [MemberController::class, 'notif'])->name('member.checkout.notify');
    });
});
